# irc-bot

Simple irc bot written in haskell, originally based on [this
guide](https://wiki.haskell.org/Roll_your_own_IRC_bot), organically
extended into its own thing.

# Building

Build with `stack build`, then copy the executable to a convenient
location (or use `stack install`).

# Running

The bot takes exactly one argument, a config file:

``` shell
  irc-bot example.config
```

One may specify the following inside the config file

  * The initial nick
  * Channel of the bot
  * The initial admin list
  * The initial ban list
  * A list of subscriptions with their subscribers

All but the nick argument are optional and both admins and bans are
hostname based; see the `example.config` for details.

There is a very primitive way to talk to IRC via the command line, so
one may control the bot if no initial admins are specified.
