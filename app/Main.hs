module Main
    ( main  -- :: IO ()
    ) where

import Cmds       qualified as Cmd
import Core.State qualified as Cmd
import Cmds.Xkcd  qualified as Xkcd

import Core.Bot (Bot (Bot, admins, allCmds, chanOpts, ignores, manager, nick, port, server, socket, subs, xkcds), CmdMap, Command (Command, prefix), Subs, emptyOpts, runNetMonad)
import Core.Connect (connectTo)
import Core.ListenAndProcess (joinAndListen)
import Core.Parser.ConfigFile.Parser (ConfigFile (Config, admins, bans, initChan, initNick, prefix, subs), pConfigFile)

import Data.Text     qualified as T
import Relude.Unsafe qualified as Unsafe

import Control.Exception (bracket)
import Network.HTTP.Conduit (newManager, tlsManagerSettings)
import Network.Socket (PortNumber)
import Prelude hiding (getArgs)
import System.Environment (getArgs)
import System.IO (hClose)
import Text.Megaparsec (ParseErrorBundle, runParser)


-- | Toplevel program: Read some input and then start the main loop.
main :: IO ()
main = (getConfig . Unsafe.head =<< getArgs) >>= \case
    Left  err -> print err
    Right cfg -> do
        let server :: String      = "irc.quakenet.org"
            port   :: PortNumber  = 6667

        -- Set up actions to start (connect), end (disconnect), and run the main
        -- loop.
        bracket (connectTo server port) hClose (mkBotAndLoop server port cfg)
  where
    -- | Parse a config file and get its contents.
    getConfig :: FilePath -> IO (Either (ParseErrorBundle Text Void) ConfigFile)
    getConfig file = runParser pConfigFile "" <$> readFileText file

    -- | Build the bot using some starting values and then run the main loop of
    -- the program.
    mkBotAndLoop server
                 port
                 cfg@Config{ initNick, initChan, admins, bans, subs, prefix }
                 handle
      = do
        print cfg
        man <- newManager tlsManagerSettings

        -- Yay for TVars!
        bnick     <- newTVarIO initNick
        comics    <- newTVarIO =<< Xkcd.dlXkcds
        admins'   <- newTVarIO $ maybeToMonoid admins
        ignores'  <- newTVarIO $ maybeToMonoid bans
        subs'     <- newTVarIO $ initSubs subs
        allCmds'  <- newTVarIO $ initCmds (fromMaybe ":" prefix) initNick
        chanOpts' <- newTVarIO $ maybe mempty (one . (, emptyOpts)) initChan

        -- Make default bot config.
        let bot = Bot
                { server    = server
                , port      = port
                , socket    = handle
                , manager   = man
                , nick      = bnick
                , admins    = admins'
                , ignores   = ignores'
                , subs      = subs'
                , allCmds   = allCmds'
                , chanOpts  = chanOpts'
                , xkcds     = comics
                }

        runNetMonad joinAndListen bot

    -- | Initial subscription commands.
    initSubs :: Maybe [(Text, Set Text)] -> Subs
    initSubs mSubs = fromList $ Cmd.bmnSub : fromMaybe [] mSubs

    -- | Initial commands the bot has to offer.
    initCmds :: Text -> Text -> CmdMap
    initCmds p bnick
      =  Cmd.ok bnick
      <> Cmd.magic8Ball bnick
      <> (keyMap (tryAddPrefix p) . mconcat $
           [ -- Admin Commands
             Cmd.dance
           , Cmd.quit
           , Cmd.quitNW
           , Cmd.joinChan
           , Cmd.part
           , Cmd.ignore
           , Cmd.addAdmin
           , Cmd.changeBotNick
           , Cmd.unignore
           , Cmd.removeAdmin
           , Cmd.butt
           , Cmd.load
           , Cmd.unload
           -- Normal Commands
           , Cmd.help'
           , Cmd.commandList
           , Cmd.proof
           , Cmd.randomDouble
           , Cmd.randomInteger
           , Cmd.randomWord
           , Cmd.schneier
           , Cmd.dad
           , Cmd.chuckNorris
           , Cmd.tell
           , Cmd.roulette
           , Cmd.bmn
           , Cmd.sub
           , Cmd.unsub
           , Cmd.subList
           , Cmd.everyone
           , Cmd.kill
           , Cmd.changePrefix
           , Cmd.about
           , Cmd.successful
           , Cmd.quote
           , Cmd.xsax
           , Cmd.mirio
           , Cmd.xkcd
           , Cmd.xkcdT
           , Cmd.xkcdNum
           , Cmd.relevantXkcd
           , Cmd.rustify
           , Cmd.cify
           , Cmd.goodBot
           , Cmd.badBot
           , Cmd.montyPython
           ])

    -- | Add a prefix to a command if none was already present.
    tryAddPrefix :: Text -> Command -> Command
    tryAddPrefix p c@Command{ prefix } =
        if T.null prefix
            then c { prefix = p }
            else c
