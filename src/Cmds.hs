-- | Reexport everything from Cmds to shorten import lists.
module Cmds
    ( module Exports
    ) where

import Cmds.ASCII.Butt           as Exports
import Cmds.ASCII.Dance          as Exports
import Cmds.ASCII.Printing       as Exports
import Cmds.About                as Exports
import Cmds.BadBot               as Exports
import Cmds.BotName.Magic8Ball   as Exports
import Cmds.BotName.Ok           as Exports
import Cmds.Cify                 as Exports
import Cmds.CommandList          as Exports
import Cmds.Everyone             as Exports
import Cmds.GoodBot              as Exports
import Cmds.Help                 as Exports
import Cmds.Jokes.BruceSchneier  as Exports
import Cmds.Jokes.ChuckNorris    as Exports
import Cmds.Jokes.Dad            as Exports
import Cmds.Jokes.MontyPython    as Exports
import Cmds.Jokes.ProofIsTrivial as Exports
import Cmds.Kill                 as Exports
import Cmds.Quote                as Exports
import Cmds.Random               as Exports
import Cmds.Roulette             as Exports
import Cmds.Rustify              as Exports
import Cmds.Subscriptions.BMN    as Exports
import Cmds.Subscriptions.Sub    as Exports
import Cmds.Successful           as Exports
import Cmds.UserTell             as Exports
import Cmds.WriteFile            as Exports
import Cmds.Xkcd                 as Exports
