-- | WARNING: Slightly NSFW.
module Cmds.ASCII.Butt
    ( butt  -- :: CmdMap
    ) where

import Cmds.ASCII.Printing (printASCII)
import Core.Bot (CmdMap, Command (execute, help, usage), Net, mkCmd, defCmd)
import Core.Message (Msg)
import Core.Util (priv, whenAdmin)


-- | Command for a printing a butt :>
butt :: CmdMap
butt = mkCmd $ defCmd
    { usage   = "butt"
    , help    = "Prints a butt \":D\".  Admin command."
    , execute = whenAdmin <*> runButt
    }

-- | Print a butt \o/
runButt :: Msg -> Net ()
runButt msg = printASCII (priv msg) buttASCII

-- | A proper ASCII butt.
buttASCII :: [Text]
buttASCII =
    [ "  \\           /"
    , "   \\         /"
    , "    )       ("
    , "  .`         `."
    , ".'             `."
    , ":        |       :"
    , "'.      .'.     .'"
    , "  \\`'''`\\ /`'''`/"
    , "   \\     |     /"
    , "    |    |    |"
    ]
