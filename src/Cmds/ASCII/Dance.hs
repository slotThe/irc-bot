module Cmds.ASCII.Dance
    ( dance  -- :: CmdMap
    ) where

import Cmds.ASCII.Printing (printASCII)
import Core.Bot (CmdMap, Command (execute, help, usage), Net, mkCmd, defCmd)
import Core.Message (Msg)
import Core.Util (priv, whenAdmin)


-- | Dance command.
dance :: CmdMap
dance = mkCmd $ defCmd
    { usage   = "dance"
    , help    = "Make the bot dance \\o/.  Admin command."
    , execute = whenAdmin <*> runDance
    }

-- | Make the bot dance :>
runDance :: Msg -> Net ()
runDance msg = printASCII (priv msg) danceMoves

-- | :)
danceMoves :: [Text]
danceMoves = ["<(^.^<)", "<(^.^)>", "(>^.^)>", "(7^.^)7", "(>^.^<)"]
