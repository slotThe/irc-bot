module Cmds.ASCII.Printing
    ( printASCII  -- :: MonadIO m => (Text -> m ()) -> [Text] -> m ()
    ) where

import Core.Bot (Net)

import UnliftIO.Concurrent (threadDelay)


-- | Print ASCII art, also delay a while so as to not get kicked for spamming.
printASCII :: MonadIO m => (Text -> m ()) -> [Text] -> m ()
printASCII printFct = traverse_ \line -> printFct line >> threadDelay 500000
{-# SPECIALISE printASCII :: (Text -> Net ()) -> [Text] -> Net () #-}
