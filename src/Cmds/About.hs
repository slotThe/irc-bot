module Cmds.About
    ( about  -- :: CmdMap
    ) where

import Core.Bot (CmdMap, Command (execute, help, usage), mkCmd, defCmd)
import Core.Util (priv)


-- | Some stuff about the bot.
about :: CmdMap
about = mkCmd $ defCmd
    { usage   = "about"
    , help    = "Let me tell you some stuff about me."
    , execute = \msg -> priv msg
          "IRC bot written in applied category theory, licensed under \
          \the AGPL-3, available at: https://gitlab.com/slotThe/irc-bot"
    }
