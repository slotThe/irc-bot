module Cmds.BadBot
    ( badBot  -- :: CmdMap
    ) where

import Core.Bot (CmdMap, Command (execute, help, prefix, usage), mkCmd, defCmd)
import Core.Message (Msg (Msg, sender))
import Core.Util (priv)


-- | Bad bot :(
badBot :: CmdMap
badBot = mkCmd $ defCmd
    { prefix  = "badbot"
    , usage   = ""
    , help    = "Was I a bad bot?"
    , execute = \msg@Msg{ sender } -> priv msg $ "I'm so sowwy " <> sender <> "!"
    }
