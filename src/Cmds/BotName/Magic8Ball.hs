module Cmds.BotName.Magic8Ball
    ( magic8Ball  -- :: Text -> CmdMap
    ) where

import Core.Bot (CmdMap, Command (execute, help, prefix, usage), Net, defCmd, mkConstraintCmd)
import Core.Util (priv)

import Data.Text qualified as T


-- | Let the bot see into the future.
magic8Ball :: Text -> CmdMap
magic8Ball p = mkConstraintCmd constraint $ defCmd
    { prefix     = p
    , usage      = ": *question*?"
    , help       = "The bot can see the future."
    , execute    = \msg -> priv msg =<< magicAnswer
    }
  where
    constraint :: Command -> Text -> Bool
    constraint c m = prefix c `T.isPrefixOf` m
                  && "?" `T.isSuffixOf` m
                  && "ok?" /= dropWhile1 (/= ' ') m

-- | Give a random answer.
magicAnswer :: Net Text
magicAnswer = randomElement answers

-- | Magic 8 ball answers.
answers :: NonEmpty Text
answers = fromList
    [ "It is certain."
    , "It is decidedly so."
    , "Without a doubt."
    , "Yes - definitely."
    , "You may rely on it."
    , "As I see it, yes."
    , "Most likely."
    , "Outlook good."
    , "Yes."
    , "Signs point to yes."
    , "Reply hazy, try again."
    , "Ask again later."
    , "Better not tell you now."
    , "Cannot predict now."
    , "Concentrate and ask again."
    , "Don't count on it."
    , "My reply is no."
    , "My sources say no."
    , "Outlook not so good."
    , "Very doubtful."
    ]
