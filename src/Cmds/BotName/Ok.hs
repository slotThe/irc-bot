module Cmds.BotName.Ok
    ( ok  -- :: Text -> CmdMap
    ) where

import Core.Bot (CmdMap, Command (execute, help, prefix, usage), defCmd, mkConstraintCmd)
import Core.Util (priv)

import Data.Text qualified as T


-- | Is everything ok?
ok :: Text -> CmdMap
ok p = mkConstraintCmd constraint $ defCmd
    { prefix     = p
    , usage      = " ok?"
    , help       = "Is everything ok?"
    , execute    = \msg -> priv msg $ (msg ^. #sender) <> ": ok."
    }
  where
    constraint :: Command -> Text -> Bool
    constraint c m = prefix c `T.isPrefixOf` m
                  && "ok?" == dropWhile1 (/= ' ') m
