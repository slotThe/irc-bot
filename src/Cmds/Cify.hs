module Cmds.Cify
    ( cify  -- :: CmdMap
    ) where

import Core.Bot (CmdMap, Command (cooldown, execute, help, usage), mkCmd, defCmd)
import Core.Message (Msg (Msg, content))
import Core.Util (priv)

import Data.Text qualified as T


-- | C-ify the input!
cify :: CmdMap
cify = mkCmd $ defCmd
    { usage   = "cify"
    , help    = "C-ify the given input :)"
    , execute = \msg@Msg{ content } -> unless (T.null content) $
          priv msg $ mconcat [ T.take (min 15 (T.length content `div` 5)) content
                             , "-Segmentation fault (core dumped)"
                             ]
    , cooldown   = 10
    }
