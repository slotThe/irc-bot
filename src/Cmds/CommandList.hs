module Cmds.CommandList
    ( commandList  -- :: CmdMap
    ) where

import Cmds.ASCII.Printing (printASCII)
import Core.Bot (CmdMap,
    Command (enabled, execute, help, prefix, usage),
    ConfigRead (getChanOpts),
    Net,
    Options (Options, cmds),
    mkCmd, defCmd,
 )
import Core.Message (Msg (Msg, channel, content))
import Core.Util (priv)

import Data.HashMap.Strict qualified as Map


-- | Command for listing all commands.
commandList :: CmdMap
commandList = mkCmd $ defCmd
    { usage   = "commands"
    , help    = "List all enabled commands.  Given the argument \"all\", list \
                \all commands, even if they are not enabled."
    , execute = runCommandList
    }

-- | List all available commands, formatted in a pretty-ish way.
runCommandList :: Msg -> Net ()
runCommandList msg = printASCII (priv msg) =<< formatCmds msg

-- | Get and format command names.
formatCmds :: ConfigRead m => Msg -> m [Text]
formatCmds Msg{ channel, content } =
    Map.lookup channel <$> getChanOpts <&> \case
        Nothing              -> ["No commands! :o"]
        Just Options{ cmds } -> lookupCmds cmds
  where
    lookupCmds :: CmdMap -> [Text]
    lookupCmds
        = chunksOf 320
        . unwords
        . intersperse " "
        . map (prefix <> usage)
        . Map.keys

          -- ability to show a certain {sub,super} set of commands.
        . case content of
              "all"   -> id
              _       -> Map.filterWithKey (\k _ -> enabled k)
{-# SPECIALIZE formatCmds :: Msg -> Net [Text] #-}
