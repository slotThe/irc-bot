module Cmds.Everyone
    ( everyone  -- :: CmdMap
    ) where

import Cmds.ASCII.Printing (printASCII)
import Core.Bot (CmdMap, 
    Bot (nick),
    Command (execute, help, prefix, usage),
    Net,
    Options (Options, nicks),
    getChanOpts,
    mkCmd, defCmd,
 )
import Core.Message (Msg (Msg, channel, sender))
import Core.Util (priv, whenAdmin)

import Data.HashMap.Strict ((!?))
import Data.Set ((\\))


-- | Annoy everyone :)
everyone :: CmdMap
everyone = mkCmd $ defCmd
    { prefix  = "@"
    , usage   = "everyone"
    , help    = "Highlight everyone in the channel."
    , execute = whenAdmin <*> highlightEveryone
    }

-- | Highlight all currently online users.
highlightEveryone :: Msg -> Net ()
highlightEveryone msg@Msg{ sender, channel } = do
    opts <- getChanOpts
    whenJust (opts !? channel) \Options{ nicks } -> do
        bnick <- readTVarIO =<< asks nick

        let onlineUsers = nicks \\ fromList [sender, bnick]
            prettySet   = chunksOf 300 . unwords $ toList onlineUsers

        printASCII (priv msg) prettySet
