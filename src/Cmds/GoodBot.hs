module Cmds.GoodBot
    ( goodBot  -- :: CmdMap
    ) where

import Core.Bot (CmdMap, Command (execute, help, prefix, usage), mkCmd, defCmd)
import Core.Message (Msg (Msg, sender))
import Core.Util (priv)


-- | Good bot :)
goodBot :: CmdMap
goodBot = mkCmd $ defCmd
    { prefix  = "goodbot"
    , usage   = ""
    , help    = "Was I a good bot?"
    , execute = \msg@Msg{ sender } -> priv msg $ "Thank you " <> sender <> ", UwU"
    }
