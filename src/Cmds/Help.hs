module Cmds.Help
    ( help'  -- :: CmdMap
    ) where

import Core.Bot (CmdMap, Command (execute, help, usage), Net, mkCmd, defCmd)
import Core.Message (Msg (Msg, channel, content))
import Core.Util (getMatched, priv)


-- | Command for getting some help.
help' :: CmdMap
help' = mkCmd $ defCmd
    { usage   = "help"
    , help    = helpHelp
    , execute = runHelp
    }

-- | Nice
helpHelp :: Text
helpHelp = "Get help on what a specific command does.  \
           \Usage: \"help <prefix><command>\".  \
           \For a list of commands, try the 'commands' command."

-- | Possibly give a user information about an asked about command.
runHelp :: Msg -> Net ()
runHelp msg@Msg{ content, channel }
    | isEmpty content = priv msg helpHelp
    | otherwise       =
        -- Display help for command if something was found.
        priv msg . maybe
            "Command not found.  For additional help, as well as a \
            \usage example, invoke the help command without any parameters."
            help
            =<< getMatched channel content
