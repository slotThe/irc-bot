module Cmds.Jokes.BruceSchneier
    ( schneier  -- :: CmdMap
    ) where

import Core.Bot (CmdMap, Command (execute, help, usage), Net, manager, mkCmd, defCmd)
import Core.Message (Msg (Msg, content))
import Core.Util (priv)

import Data.Text qualified as T

import Data.Char (toUpper)
import Network.HTTP.Conduit (Request, httpLbs, method, responseBody, responseStatus)
import Network.HTTP.Types.Method (methodGet)
import Text.HTML.Parser (Attr (Attr), Token (TagClose, TagOpen), parseTokens)
import Text.HTML.Parser.Util (allContentText, between)


-- | Command for a schneier fact.
schneier :: CmdMap
schneier = mkCmd $ defCmd
    { usage = "joke"
    , help  = "Display a fact about Bruce Schneier.  Optionally replace the \
              \name with a given argument."
    , execute = runSchneier
    }

-- | Function to execute a schneier fact.
runSchneier :: Msg -> Net ()
runSchneier msg@Msg{ content } = priv msg =<< schneierFact content

-- | Quick type for what's essentially a strict tuple.
data Name = Name !Text !Text

-- | Get a schneier fact from the web.
schneierFact :: Text -> Net Text
schneierFact name = schneierFact' $
    case nonEmpty . words . T.strip $ name of
        Nothing       -> Name "Bruce" "Schneier"
        Just fullName -> Name (head fullName)
                              (maybe "" head (nonEmpty $ tail fullName))

-- | Get a schneier fact.
schneierFact' :: Name -> Net Text
schneierFact' (Name fn ln) = do
    let request = schneierURL { method = methodGet }
    man <- asks manager
    (resB, res) <-
        catch ((responseBody &&& Just) <$> httpLbs request man)
              (\(_ :: SomeException) -> pure ( "Failed to connect to website.  \
                                               \It's probably down, if not please \
                                               \scream at the author."
                                             , Nothing))
    pure $! if   maybe False (checkResult . responseStatus) res
            then formatSchneier fn ln
               . mconcat
               . allContentText
               . between factOpen (TagClose "p")
               . parseTokens
               . decodeUtf8
               $ resB
            else "Failed to connect to website.  It's probably down, \
                 \if not please scream at the author."
  where
    factOpen :: Token
    factOpen = TagOpen "p" [Attr "class" "fact"]  -- Start of fact.

-- | Properly format a joke.  Basically a whole bunch of replacements.
formatSchneier :: Text -> Text -> Text -> Text
formatSchneier firstName lastName
    -- Fix things that the name replacement possibly killed
    = T.replace "  "     " "
    . T.replace " 's"    "'s"
    . T.replace " ."     "."
    . replaceName

    -- Escaped stuff
    . T.replace "&gt;"   ">"
    . T.replace "&lt;"   "<"
    . T.replace "&quot;" "\""
    . T.replace "&#39;"  "'"
    . T.replace "&#x27;" "'"
    . T.replace "\n"     ""
    . T.replace "\r"     ""
  where
    -- | All things involving name replacements.
    replaceName :: Text -> Text
    replaceName
        = T.replace "BS"         capitalFirst -- TODO
        . T.replace "BRUCE"      (T.map toUpper firstName)
        . T.replace "Bruce"      firstName
        . T.replace "SCHNEIER"   (T.map toUpper lastName)
        . T.replace "Schneier"   lastName
        . T.replace "Schneier's" (lastName <> ending)

    capitalFirst :: Text
    capitalFirst = one . toUpper . T.head $ firstName

    ending :: Text
    ending = decideEnding firstName lastName

-- | URL to get Schneier facts from.
schneierURL :: Request
schneierURL = "https://www.schneierfacts.com/"
