{-# LANGUAGE DeriveAnyClass #-}
module Cmds.Jokes.ChuckNorris
    ( chuckNorris  -- :: CmdMap
    ) where

import Prelude hiding ((.:))

import Core.Bot (CmdMap, Command (execute, help, usage), Net, manager, mkCmd, defCmd)
import Core.Message (Msg (Msg, content))
import Core.Util (priv)

import Data.Text qualified as T

import Data.Aeson (FromJSON, Value (Object), decode', parseJSON, (.:))
import Data.Char (toUpper)
import Network.HTTP.Conduit (Request, httpLbs)
import Network.HTTP.Simple (getResponseBody)


-- | Chuck Norris command.
chuckNorris :: CmdMap
chuckNorris = mkCmd $ defCmd
    { usage = "jokeCN"
    , help  = "Display a fact about Chuck Norris.  Optionally replace the \
              \name with a given argument."
    , execute = runChuckNorris
    }

-- | Run the Chuck Norris command.
runChuckNorris :: Msg -> Net ()
runChuckNorris msg@Msg{ content } = priv msg =<< chuckNorris' content

-- | Data types needed for JSON parsing.
data Joke = Joke
    { success :: !Text
    , value   :: ![InnerJoke]
    } deriving stock (Show)

-- | Manually specify `FromJSON` instance due to dumb field names.
instance FromJSON Joke where
    parseJSON (Object v) =
        Joke <$> v .: "type"
             <*> v .: "value"
    parseJSON _ = mzero

-- | Actual joke.
data InnerJoke = InnerJoke
    { id         :: !Int
    , joke       :: !Text
    , categories :: ![Text]
    }
    deriving stock    (Show, Generic)
    deriving anyclass (FromJSON)

-- | An empty joke.
emptyInnerJoke :: InnerJoke
emptyInnerJoke = InnerJoke 0 "" []

-- | Website to get bad jokes from.
jsonURL :: Request
jsonURL = "http://api.icndb.com/jokes/random/1"

-- | Quick type for what's essentially a strict tuple.
data Name = Name !Text !Text

chuckNorris' :: Text -> Net Text
chuckNorris' name = chuckNorris'' $
    case nonEmpty . words . T.strip $ name of
        Nothing       -> Name "Chuck" "Norris"
        Just fullName -> Name (head fullName)
                              (maybe "" head (nonEmpty $ tail fullName))

-- | Decode JSON into above defined data types, replace all occurences of "Chuck
-- Norris" with the first input word.
chuckNorris'' :: Name -> Net Text
chuckNorris'' (Name firstName lastName) = do
    man <- asks manager
    catch
        (decode' . getResponseBody <$> httpLbs jsonURL man <&> \case
            -- Bail out if something went wrong.
            Nothing -> "Site is either offline or the JSON was malformed."
            Just j  -> format
                     . joke
                     . maybe emptyInnerJoke head
                     . nonEmpty
                     . value
                    $! j)

        -- Bail out if something went very wrong.
        \(_ :: SomeException) ->
            pure "Failed to connect to website.  It's probably down, if not \
                 \please scream at the author."
  where
    capitalFirst :: Text
    capitalFirst = one . toUpper . T.head $ firstName

    ending :: Text
    ending = decideEnding firstName lastName

    format :: Text -> Text
    format = T.replace "  " " "
           . T.replace " 's"     "'s"
           . T.replace "CN"      capitalFirst  -- TODO
           . T.replace "Chuck"   firstName
           . T.replace "Norris"  lastName
           . T.replace "Norris'" (lastName <> ending)
           . T.replace "&quot;"  "\""
