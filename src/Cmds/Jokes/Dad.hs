module Cmds.Jokes.Dad
    ( dad  -- :: CmdMap
    ) where

import Core.Bot (CmdMap, Command (execute, help, usage), Net, manager, mkCmd, defCmd)
import Core.Message (Msg)
import Core.Util (priv)

import Data.Text qualified as T

import Data.Text.Encoding (decodeLatin1)
import Network.HTTP.Conduit (Request (requestHeaders), httpLbs, responseBody, responseStatus)


-- | Command for a dad joke.
dad :: CmdMap
dad = mkCmd $ defCmd
    { usage   = "dad"
    , help    = "The best dad jokes in town, served hot."
    , execute = runDad
    }

-- | Function to get a dad joke and print it.
runDad :: Msg -> Net ()
runDad msg = priv msg =<< dadJoke

{- | Get a dad joke from a website.
   For this, make a simple GET request for plain text.  Format string
   afterwards.
-}
dadJoke :: Net Text
dadJoke = do
    let request = dadURL { requestHeaders = [("Accept", "text/plain")] }
    man <- asks manager
    res <- httpLbs request man  -- make the tls connection

    pure $! if checkResult (responseStatus res)
        then removeLineBreaks . decodeLatin1 . toStrict . responseBody $ res
        else "Failed to connect to website.  It's probably down, \
             \if not please scream at the author."
  where
    removeLineBreaks :: Text -> Text
    removeLineBreaks = T.replace "\r" "" . T.replace "\n" " "

-- | URL with bad dad jokes.
dadURL :: Request
dadURL = "https://icanhazdadjoke.com/"
