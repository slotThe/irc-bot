module Cmds.Jokes.ProofIsTrivial
    ( proof  -- :: CmdMap
    ) where

-- Source: https://github.com/alangpierce/TheProofIsTrivial

import Core.Bot (CmdMap, Command (execute, help, usage), Net, mkCmd, defCmd)
import Core.Message (Msg)
import Core.Util (priv)

import Data.Text qualified as T


-- | Command for a proof.
proof :: CmdMap
proof = mkCmd $ defCmd
    { usage   = "proof"
    , help    = "I hope you don't feel intellectually insulted knowing \
                 \there is a help text to this function."
    , execute = runProof
    }

-- | Construct a trivial proof.
runProof :: Msg -> Net ()
runProof msg = priv msg =<< theProofIsTrivial

-- | The meaning of this function should be insultingly obvious to even the most
-- casual of observers.
theProofIsTrivial :: Net Text
theProofIsTrivial = do
    -- Get a whole bunch of random elements.
    intro   <- randomElement intros
    fstAdj  <- randomElement adjectives
    sndAdj  <- randomElement adjectives
    fstNoun <- randomElement setNouns
    sndNoun <- randomElement allNouns

    pure $! mconcat . intersperse " " $
        [ "The proof is trivial!"
        , intro <> firstVocal fstAdj  -- Intro sentence, respecting vocals.
        , fstAdj
        , singular fstNoun
        , "whose elements are"
        , sndAdj
        , plural sndNoun
        ]

-- | Check if the first letter of a string is a vowel.
firstVocal :: Text -> Text
firstVocal str
    | (T.head str ==) `T.any` "aeiou" = "n"
    | otherwise                       = ""

intros :: NonEmpty Text
intros = fromList ["Just biject it to a", "Just view the problem as a"]

-- | Mathematical terms can have a singular or a plural.
data Term = Term
    { singular :: !Text
    , plural   :: !Text
    }

-- | Create a term form a singular if no special plural form is known.
mkTermFromSingular :: Text -> Term
mkTermFromSingular "" = Term "" ""
mkTermFromSingular s  = Term s  getEnding
  where
    getEnding :: Text
    getEnding
        | T.last s == 's' = s <> "es"
        | otherwise = s <> "s"

adjectives :: NonEmpty Text
adjectives = fromList
    [ "abelian"
    , "associative"
    , "computable"
    , "Lebesgue-measurable"
    , "semi-decidable"
    , "simple"
    , "combinatorial"
    , "structure-preserving"
    , "diagonalizable"
    , "nonsingular"
    , "orientable"
    , "twice-differentiable"
    , "thrice-differentiable"
    , "countable"
    , "prime"
    , "complete"
    , "continuous"
    , "trivial"
    , "3-connected"
    , "bipartite"
    , "planar"
    , "finite"
    , "nondeterministic"
    , "alternating"
    , "convex"
    , "undecidable"
    , "dihedral"
    , "context-free"
    , "rational"
    , "regular"
    , "Noetherian"
    , "Cauchy"
    , "open"
    , "closed"
    , "compact"
    , "clopen"
    , "pointless"
    , "perfect"
    , "non-degenerate"
    , "degenerate"
    , "skew-symmetric"
    , "sesquilinear"
    , "fundamental"
    , "smooth"
    , "connected"
    , "simplicial"
    , "universal"
    , "greedy"
    , "normal"
    , "total"
    , "left invertible"
    , "exact"
    , "empty"
    ]

setNouns :: NonEmpty Term
setNouns = fromList $ map mkTermFromSingular
    [ "multiset"
    , "metric space"
    , "group"
    , "monoid"
    , "semigroup"
    , "ring"
    , "field"
    , "module"
    , "topological space"
    , "Hilbert space"
    , "manifold"
    , "hypergraph"
    , "DAG"
    , "residue class"
    , "logistic system"
    , "complexity class"
    , "language"
    , "poset"
    , "algebra"
    , "Lie algebra"
    , "Dynkin system"
    , "sigma-algebra"
    , "ultrafilter"
    , "Cayley graph"
    , "variety"
    , "orbit"
    ]

-- | Mathematical objects that can't contain elements (unless you're a set
-- theorist).
allNouns :: NonEmpty Term
allNouns = (setNouns <>) . fromList $
    ( -- Special Plural forms are known.
       map (uncurry Term)
           [ ("taylor series", "taylor series")
           , ("pushdown automaton", "pushdown automata")
           ]
    ++ map mkTermFromSingular
           [ "integer"
           , "Turing machine"
           , "automorphism"
           , "bijection"
           , "generating function"
           , "linear transformation"
           , "combinatorial game"
           , "equivalence relation"
           , "tournament"
           , "random variable"
           , "triangulation"
           , "unbounded-fan-in circuit"
           , "log-space reduction"
           , "Markov chain"
           , "4-form"
           , "7-chain"
           , "operator"
           , "homeomorphism"
           , "color"
           , "Betti number"
           , "Radon-Nikodym derivative"
           ])
