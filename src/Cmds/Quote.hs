{-# LANGUAGE DeriveAnyClass #-}
module Cmds.Quote
    ( quote  -- :: CmdMap
    , xsax   -- :: CmdMap
    , mirio  -- :: CmdMap
    ) where

import Cmds.ASCII.Printing (printASCII)
import Core.Bot (CmdMap, Command (cooldown, execute, help, usage), Net, manager, mkCmd, defCmd)
import Core.Message (Msg (Msg), content)
import Core.Util (priv)

import Data.Text qualified as T

import Data.Aeson (FromJSON, eitherDecode)
import Data.Time (secondsToNominalDiffTime)
import Network.HTTP.Conduit (httpLbs, parseUrlThrow)
import Network.HTTP.Simple (getResponseBody)
import UnliftIO (fromEither)


-- | Fetch a random quote from quotedb.
quote :: CmdMap
quote = mkCmd $ defCmd
    { usage = "quote"
    , help  = "Display a random quote from the xonotic quotedb.  \
              \Takes an optional argument to fetch quotes from a \
              \specific person."
    , execute  = runQuote
    , cooldown = secondsToNominalDiffTime 20
    }

-- | It had to be done...
xsax :: CmdMap
xsax = mkCmd $ defCmd
    { usage   = "xsax"
    , help    = "axaxaxaxaxaxaxaxaxaxaxax"
    , execute = \msg -> runQuote msg{ content = "XSAX" }
    }

mirio :: CmdMap
mirio = mkCmd $ defCmd
    { usage   = "mimimi"
    , help    = "mimimi"
    , execute = \msg -> runQuote msg{ content = "Mirio" }
    }

-- | Run the quote command.
runQuote :: Msg -> Net ()
runQuote msg@Msg{ content } =
    -- Print quote with a time delay in case of long quotes (so the bot does not
    -- get kicked from the server).
    printASCII (priv msg) . T.splitOn "\n" =<< quoteType content

-- | Many quotes.
newtype Quotes = Quotes { quotes :: [Quote] }
  deriving stock    (Generic)
  deriving anyclass (FromJSON)

-- | Single quote.
data Quote = Quote
    { owner     :: Text
    , index     :: Int
    , create_dt :: Text
    , text      :: Text
    , nick      :: Text
    }
    deriving stock    (Generic)
    deriving anyclass (FromJSON)

-- | Quotes can either be arbitrary or from someone specific.
quoteType :: Text -> Net Text
quoteType name = getQuote $!
    quoteURL <> (if isEmpty name then "" else "nick/" <> userName) <> "?json=1"
  where
    userName :: Text
    userName = T.takeWhile (/= ' ') name

-- | Connect to the quotedb and parse the quotes, then return a random one.
getQuote :: Text -> Net Text
getQuote url = do
    man <- asks manager
    catch
        do request <- fromEither $ parseUrlThrow (toString url)
           httpLbs request man <&> eitherDecode . getResponseBody >>= \case
               Left err          -> pure $ toText err
               Right (Quotes qs) -> case nonEmpty (noBadQuotes qs) of
                   Nothing  -> pure "No quotes for this user... :("
                   Just qs' -> mimimi . quoteText <$> randomElement qs'

        -- If anything fails, just bail out and print an error message.
        \(_ :: SomeException) ->
            pure "Failed to connect to website.  It's probably down, if not \
                  \please scream at the author."
  where
    quoteText :: Quote -> Text
    quoteText = text

    -- | Jesus christ
    noBadQuotes :: [Quote] -> [Quote]
    noBadQuotes = filter ((/= "LegendaryGuard") . owner)

-- | Mimimimimimimimimimimimimimimimimimi
mimimi :: Text -> Text
mimimi = T.replace "Mirio" "Mimimio"

quoteURL :: Text
quoteURL = "http://54.93.182.54:27600/"

-- | This might be needed later.
-- censorName :: Text -> Text
-- censorName name = maybe name censorHead (T.uncons name)
--   where
--     censorHead :: (Char, Text) -> Text
--     censorHead ('<', t) = maybe name (("<*" <>) . snd) (T.uncons t)
--     censorHead _        = name
