module Cmds.Random
    ( randomInteger  -- :: CmdMap
    , randomDouble   -- :: CmdMap
    , randomWord     -- :: CmdMap
    ) where

import Core.Bot (CmdMap, Command (execute, help, usage), Net, mkCmd, defCmd)
import Core.Message (Msg (Msg, content))
import Core.Util (priv)

import Data.Text qualified as T

import Data.Char (isDigit, isSpace)


-- | Command for picking a random word.
randomWord :: CmdMap
randomWord = mkCmd $ defCmd
    { usage   = "randomW"
    , help    = wordHelp
    , execute = runRandomWord
    }

wordHelp :: Text
wordHelp = "Takes words, picks a random one and returns it.  Different \
           \entries may also be separated with a semicolon (;), allowing \
           \one to pick sentences.  Usage: \"randomW foo bar baz\" or\
           \ \"randomW vi vi vi, the editor of the beast ; malice sucks\""

-- | Pick a random word.
runRandomWord :: Msg -> Net ()
runRandomWord msg@Msg{ content } = priv msg =<< randomString content

-- | Command for picking a random integer.
randomInteger :: CmdMap
randomInteger = mkCmd $ defCmd
    { usage   = "random"
    , help    = integerHelp
    , execute = runRandomInteger
    }

integerHelp :: Text
integerHelp = "Takes bounds, returns random number from within those bounds.\
              \  Usage: \"random 10 20\""

-- | Pick a random integer.
runRandomInteger :: Msg -> Net ()
runRandomInteger msg@Msg{ content } = priv msg =<< randomRangeInteger content

-- | Command for picking a random double.
randomDouble :: CmdMap
randomDouble = mkCmd $ defCmd
    { usage   = "randomF"
    , help    = doubleHelp
    , execute = runRandomDouble
    }

doubleHelp :: Text
doubleHelp = "Takes bounds, returns random float from within those bounds.\
             \  The number of decimal places used is given by the absolute\
             \ number(i.e. their length as a string) of decimal places of\
             \ the input numbers.  Usage: \"randomF 0.3 0.5\""


-- | Pick a random double.
runRandomDouble :: Msg -> Net ()
runRandomDouble msg@Msg{ content } = priv msg =<< randomRangeDouble content

-- | Either pick a random word from the given string, or pick a random phrase
-- (where phrases are separated by /;/).
randomString :: Text -> Net Text
randomString str =
    case T.strip <$> T.splitOn ";" str of
        []         -> pure ""  -- this should never happen
        xs@(x:xs') ->
            if | T.null x  -> pure wordHelp                       -- empty string
               | null xs'  -> randomElement . fromList $ words x  -- single words
               | otherwise -> randomElement $ fromList xs         -- phrases
               -- these 'fromList' calls are safe, as we already know that 'x'
               -- resp. 'xs' is non-empty.

-- | Integer random numbers.
randomRangeInteger :: Text -> Net Text
randomRangeInteger str
    | isNothing checkIntegers = pure integerHelp
    | otherwise               = show <$> randomNumber pIntegers
  where
    -- | The parsed integers.
    pIntegers :: (Integer, Integer)
    pIntegers =
        fromMaybeTuple (fst3 <$> checkIntegers, snd3 <$> checkIntegers)

    -- | Check if conditions are satisfied.
    checkIntegers :: Maybe (Integer, Integer, Int)
    checkIntegers = randomCheckConds @Integer ints

    -- | Get integers from string.
    ints :: [Text]
    ints = words $ T.filter digitSpace str

    -- | Integers consist of digits, separated with spaces.
    digitSpace :: Char -> Bool
    digitSpace c = any ($ c) [isDigit, isSpace]

-- | Random numbers for Doubles.
randomRangeDouble :: Text -> Net Text
randomRangeDouble str
    | isNothing checkDoubles = pure doubleHelp
    | otherwise              = showShortened <$> randomNumber pDoubles
  where
    -- | Parsed doubles
    pDoubles :: (Double, Double)
    pDoubles = fromMaybeTuple (fst3 <$> checkDoubles, snd3 <$> checkDoubles)

    -- | Check is conditions are satisfied.
    checkDoubles :: Maybe (Double, Double, Int)
    checkDoubles = randomCheckConds @Double dbles

    -- | Get numbers from string.
    dbles :: [Text]
    dbles = words $ T.filter digitDotSpace str

    -- | Doubles consist of digits and dots, separated by spaces.
    digitDotSpace :: Char -> Bool
    digitDotSpace c = any ($ c) [isDigit, (== '.'), isSpace]

    -- | Show the number up until the required number of decimal places.
    showShortened :: Double -> Text
    showShortened = show . (`numDecPlaces` maxDecPlaces)

    -- | Max decimal places to show based on input numbers.
    maxDecPlaces :: Int
    maxDecPlaces = maybe 0 thrd3 checkDoubles

-- | Check necessary conditions.
randomCheckConds :: Read a => [Text] -> Maybe (a, a, Int)
randomCheckConds str = do
    firstNumber  <- viaNonEmpty head str
    secondNumber <- head <$> (nonEmpty . tail =<< nonEmpty str)

    f <- tmaybeRead firstNumber
    s <- tmaybeRead secondNumber

    pure (f, s, getDigitLength firstNumber `max` getDigitLength secondNumber)
  where
    getDigitLength :: Text -> Int
    getDigitLength = T.length . dropWhile1 (/= '.')

-- | Convert a tuple of maybes into a normal tuple with a reasonable default
-- value (for this application).
fromMaybeTuple :: (Num a, Num b) => (Maybe a, Maybe b) -> (a, b)
fromMaybeTuple (a, b) = (fromMaybe 0 a, fromMaybe 0 b)
