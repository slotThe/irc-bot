module Cmds.Roulette
    ( roulette  -- :: CmdMap
    ) where

import Cmds.Kill (runKill)
import Core.Bot (CmdMap, 
    Command (execute, help, usage),
    Net,
    Options (Options, nicks),
    getChanOpts,
    mkCmd, defCmd,
    nick,
 )
import Core.Message (Msg (Msg, channel))

import Data.HashMap.Strict ((!?))


-- | Roulette command.
roulette :: CmdMap
roulette = mkCmd $ defCmd
    { usage   = "roulette"
    , help    = "Kill a random person in the channel."
    , execute = runRoulette
    }

-- | Kill a random person in the channel.
runRoulette :: Msg -> Net ()
runRoulette msg@Msg{ channel } = do
    opts <- getChanOpts
    whenJust (opts !? channel) \Options{ nicks } -> do
        target <- randomMember nicks
        bot    <- readTVarIO =<< asks nick
        runKill $ msg & #sender  .~ bot
                      & #content .~ target
