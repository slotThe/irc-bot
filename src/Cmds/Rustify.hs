module Cmds.Rustify
    ( rustify  -- :: CmdMap
    ) where

import Core.Bot (CmdMap, Command (cooldown, execute, help, usage), mkCmd, defCmd)
import Core.Message (Msg (Msg, content))
import Core.Util (priv)

import Data.Text qualified as T


-- | Rustify the input!
rustify :: CmdMap
rustify = mkCmd $ defCmd
    { usage   = "rustify"
    , help    = "Rustify the given input :)"
    , execute = \msg@Msg{ content } -> unless (T.null content) $
          priv msg $ mconcat [ "unsafe { "
                             , T.take 75 content
                             , ".unwrap().unwrap().unwrap() }"
                             ]
    -- No spammerino
    , cooldown   = 10
    }
