module Cmds.Subscriptions.BMN
    ( bmn     -- :: CmdMap
    , bmnSub  -- :: (Text, Set Text)
    ) where

import Core.Bot (CmdMap,
    Command (execute, help, usage),
    ConfigRead (getChanOpts, getSubs),
    Net,
    Options (Options, nicks),
    mkCmd, defCmd,
 )
import Core.Message (Msg (Msg, channel, content, sender))
import Core.Util (priv)

import Data.HashMap.Strict qualified as Map
import Data.Set qualified            as Set

import Data.HashMap.Strict ((!?))

-- | The initial state of the BMN sub.
bmnSub :: (Text, Set Text)
bmnSub = ("bmn", mempty)

-- | Command for highlighting all users who have subscribed to the bmn list.
bmn :: CmdMap
bmn = mkCmd $ defCmd
    { usage   = "bmn"
    , help    = "Highlight all users who have subscribed to the BMN list."
    , execute = highlightBmn
    }

-- | Highlight all currently online users that have subscribed to the bmn list.
highlightBmn :: Msg -> Net ()
highlightBmn msg@Msg{ content, sender, channel } = do
    opts <- getChanOpts

    whenJust (opts !? channel) \Options{ nicks } ->
        -- Get all subscribers for the 'bmn' command.
        getSubs <&> Map.lookup (fst bmnSub) >>= \case
            Nothing  -> priv msg "Command not found.  What have you done?"
            Just sbs -> do
                    -- Online users minus the person sending the message.
                let onlineUsers = Set.delete sender $ sbs `Set.intersection` nicks
                    prettySet   = unwords $ toList onlineUsers

                priv msg $ prettySet <> ": " <> content
