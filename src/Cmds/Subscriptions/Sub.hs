module Cmds.Subscriptions.Sub
    ( sub              -- :: CmdMap
    , unsub            -- :: CmdMap
    , subList          -- :: CmdMap
    , changeNameInSub  -- :: Text -> Text -> Subs -> Subs
    ) where

import Core.Bot (CmdMap,
    Command (execute, help, usage),
    ConfigRead,
    ConfigWrite (modifySubs),
    Net,
    Subs,
    getSubs,
    mkCmd, defCmd,
 )
import Core.Message (Msg (Msg, content, sender))
import Core.Util (priv)

import Data.HashMap.Strict qualified as Map
import Data.Set qualified            as Set
import Data.Text qualified           as T


-- | Was a certain sub found in the set of subs?
type MatchFound = Bool

-- | Command to let a user subscribe to a certain sub list.
sub :: CmdMap
sub = mkCmd $ defCmd
    { usage = "sub"
    , help  = "Subscribe to a certain sub list and be notified when \
              \something is happening.  Usage: sub SUBNAME.  \
              \For a list of subscriptions, use `sublist`."
    , execute = tryToSub
    }

subHelp :: Text
subHelp = "Subscribe to a certain sub list and be notified when \
          \something is happening.  Usage: sub SUBNAME.  \
          \For a list of subscriptions, use `sublist`."

-- | Command to let a user unsubscribe from a certain sub list.
unsub :: CmdMap
unsub = mkCmd $ defCmd
   { usage = "unsub"
   , help  = "Unsubscribe from a certain sub list.  \
             \Usage: unsub SUBNAME.  For a list of subscriptions, use `sublist`."
   , execute = tryToUnsub
   }

-- | Command to list all available sub lists.
subList :: CmdMap
subList = mkCmd $ defCmd
    { usage   = "sublist"
    , help    = "List all subscriptions that users that sub to."
    , execute = listSubs
    }

-- | Try to subscribe a user to a certain sub list.
tryToSub :: Msg -> Net ()
tryToSub = doSub trySubbing "subscribed!"
  where trySubbing :: Msg -> Net MatchFound
          = modSubWith Set.insert

-- | Try to unsubcribe a user to a certain sub list.
tryToUnsub :: Msg -> Net ()
tryToUnsub = doSub tryUnsubbing "unsubscribed!"
  where tryUnsubbing :: Msg -> Net MatchFound
          = modSubWith Set.delete

-- | Generic function for doing things with subs.
doSub :: (Msg -> Net MatchFound)  -- ^ Sub or unsub?
      -> Text                     -- ^ Confirmation message.
      -> Msg
      -> Net ()
doSub subFun conf msg@Msg{ content }
    | isEmpty content = priv msg subHelp
    | otherwise       = whenM (subFun msg') $ priv msg conf  -- check for success
  where
    msg' :: Msg
    msg' = msg & #content %~ T.takeWhile (/= ' ')

-- | Print all subscriptions.
listSubs :: Msg -> Net ()
listSubs msg = priv msg . unwords . Map.keys =<< getSubs

-- | Try to apply a sub function, modify the bot state if the sub actually
-- existed and return a confirmation whether anything happened.
modSubWith
    :: (ConfigRead m, ConfigWrite m)
    => (Text -> Set Text -> Set Text)
    -- ^ Function with which to manipulate the sub.
    -> Msg
    -- ^ Msg with the name of the sender and the subscription.
    -> m MatchFound
    -- ^ Config state change plus an indicator if it was successful.
modSubWith subFun Msg{ content = subName, sender } = do
    subs <- getSubs
    if subName `Map.member` subs
        then True <$ modifySubs (Map.adjust (subFun sender) subName)
        else pure False
{-# SPECIALIZE modSubWith :: (Text -> Set Text -> Set Text) -> Msg -> Net Bool #-}

-- | Change the name of a user (i.e. in case of a name change on IRC) in the
-- subs.
changeNameInSub
    :: Text  -- ^ Old name of user.
    -> Text  -- ^ New name of user.
    -> Subs   -- ^ The sub to change the names in.
    -> Subs   -- ^ The new sub with the names replaced.
changeNameInSub old new = Map.adjust (new `replaces` old) old
