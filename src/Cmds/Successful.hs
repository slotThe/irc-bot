module Cmds.Successful
    ( successful  -- :: CmdMap
    ) where

import Core.Bot (CmdMap, CmdMap, Command (execute, help, prefix, usage), defCmd, mkCmd)
import Core.Util (priv)


-- | Successful command.
successful :: CmdMap
successful = mkCmd $ defCmd
    { prefix  = "<--"
    , usage   = " successful"
    , help    = "Was it successful?"
    , execute = \msg -> priv msg "--> very"
    }
