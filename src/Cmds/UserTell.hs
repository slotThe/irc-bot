module Cmds.UserTell
    ( makeNewTell  -- :: Msg -> Net ()
    , checkTells   -- :: Msg -> Net ()
    , tell         -- :: CmdMap
    ) where

import Core.Bot (CmdMap, CmdMap, Command (execute, help, usage), Net, Options (Options, nicks, tells), defCmd, getChanOpts, mkCmd, modifyOpts)
import Core.Message (Msg (Msg, channel, content, sender))
import Core.Util (priv, whenI)

import Data.HashMap.Strict qualified as Map
import Data.Set qualified            as Set
import Data.Text qualified           as T

import Data.HashMap.Strict ((!?))


-- | Command for a tell.
tell :: CmdMap
tell = mkCmd $ defCmd
    { usage   = "tell"
    , help    = tellHelp
    , execute = makeNewTell
    }

tellHelp :: Text
tellHelp = "Tell an offline user something.  The user will receive the \
           \tell as soon as they join the channel.  Usage: 'tell NICK MESSAGE'."

{- | Check if there is a tell waiting for the given user.
   This is executed when a new user joins the channel, so `sender` is exactly
   what we want here.
-}
checkTells :: Msg -> Net ()
checkTells msg@Msg{ sender = recipient, channel } = do
    opts <- getChanOpts
    whenJust (opts !? channel) \Options{ tells } ->
        whenJust (tells !? recipient) \tellsForUser -> do
            -- Print all tells for a given nickname.
            traverse_ (priv msg . makeTellMsg recipient) tellsForUser

            -- Remove the just printed tells.
            modifyOpts $ Map.adjust (#tells %~ Map.delete recipient) channel

-- | Tell an offline user something.
makeNewTell :: Msg -> Net ()
makeNewTell msg@Msg{ content, channel }
    | isEmpty content = priv msg tellHelp
    | otherwise       = do
        opts <- getChanOpts
        whenJust (opts !? channel) \Options{ nicks } ->
            -- If the person is already in the channel, print an insult instead.
            whenI (not $ recipient `Set.member` nicks) msg $ do
                -- Otherwise add the message to the set of tells and confirm.
                modifyOpts $
                    Map.adjust (#tells %~ Map.insertWith (<>) recipient (one msg'))
                               channel
                priv msg "It is noted."
  where
    recipient :: Text
    recipient = T.takeWhile (/= ' ') content  -- Tell this person the message.

    msg' :: Msg
    msg' = msg & #content %~ dropWhile1 (/= ' ')

-- | The message that the bot tells the users.
makeTellMsg :: Text -> Msg -> Text
makeTellMsg recipient Msg{ sender, content } = mconcat
    [recipient, ", message for you from ", sender, ": ", content]
