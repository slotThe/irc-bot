module Cmds.WriteFile
    ( quit  -- :: CmdMap
    ) where

import Core.Bot (CmdMap, 
    Command (execute, help, usage),
    ConfigRead (getAdmins, getChanOpts, getIgnores, getNick, getSubs),
    Net,
    mkCmd, defCmd,
 )
import Core.Parser.ConfigFile.Parser (
    startAdmins,
    startBans,
    startChan,
    startNick,
    startSubs,
 )
import Core.State (runQuit)
import Core.Util (whenAdmin)

import Data.HashMap.Strict qualified as Map
import Data.Text qualified           as T


-- | Quit from IRC and write some bot state to a config file.
quit :: CmdMap
quit = mkCmd $ defCmd
    { usage = "quit"
    , help  = "Quit from IRC and write some bot state to a file.  \
              \Admin command."
    , execute = \msg -> whenAdmin msg $ writeCfgToFile *> runQuit
    }

-- | Save a config file.
writeCfgToFile :: Net ()
writeCfgToFile = do
    admins <- getAdmins
    let adminsW = startAdmins <> " " <> unwords (toList admins)

    chans <- Map.keys <$> getChanOpts
    let chanW = maybe "" (\chan -> startChan <> " " <> chan) (listToMaybe chans)

    ignores <- getIgnores
    let ignoresW = startBans <> " " <> unwords (toList ignores)

    nick <- getNick
    let nickW = startNick <> " " <> nick

    subs <- toList <$> getSubs
    let subsW = startSubs <> "\n" <> mconcat (map formatSub subs)

    -- Put everything together
    let cfg = T.intercalate "\n" [nickW, chanW, adminsW, ignoresW, subsW]

    -- Actually write to the file
    writeFileText "bot.config" cfg

  where
    formatSub :: (Text, Set Text) -> Text
    formatSub (name, subs) = mconcat
        [ "  "
        , name
        , ": "
        , unwords $ toList subs
        , "\n"
        ]
