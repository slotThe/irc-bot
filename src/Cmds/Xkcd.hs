module Cmds.Xkcd
    ( xkcd               -- :: CmdMap
    , xkcdT              -- :: CmdMap
    , xkcdNum            -- :: CmdMap
    , relevantXkcd       -- :: CmdMap

    , dlXkcds            -- :: IO (Set Xkcd)
    , checkForNewComics  -- :: Net ()
    ) where

import Core.Bot (CmdMap,
  CmdMap,
  Command (execute, help, usage),
  ConfigRead (getManager, getXkcds),
  Net,
  Xkcd (Xkcd, alt, img, num, title, transcript),
  defCmd,
  mkCmd,
  modifyXkcds,
 )
import Core.Message (Msg (Msg, content))
import Core.Util (priv)

import Data.Set qualified  as Set
import Data.Text qualified as T

import Data.Aeson (decode)
import Network.HTTP.Conduit (
    Manager,
    httpLbs,
    newManager,
    parseUrlThrow,
    responseBody,
    tlsManagerSettings,
 )
import UnliftIO (fromEither, mapConcurrently)
import UnliftIO.Concurrent (threadDelay)


xkcd :: CmdMap
xkcd = mkCmd $ defCmd
    { usage   = "xkcd"
    , help    = "Get xkcd by title.  Return the most up-to-date comic on an \
                \empty input."
    , execute = getWith byTitle
    }
  where
    byTitle :: Text -> Xkcd -> Bool
    byTitle query Xkcd{ title } = T.toCaseFold query `T.isInfixOf` T.toCaseFold title

xkcdT :: CmdMap
xkcdT = mkCmd $ defCmd
    { usage   = "xkcdT"
    , help    = "Get xkcd by words occuring in the transcript (if present).  \
                \Return the most up-to-date comic on an empty input."
    , execute = getWith byTranscript
    }
  where
    byTranscript :: Text -> Xkcd -> Bool
    byTranscript query Xkcd{ transcript } =
        T.toCaseFold query `T.isInfixOf` T.toCaseFold transcript

xkcdNum :: CmdMap
xkcdNum = mkCmd $ defCmd
    { usage   = "xkcdNum"
    , help    = "Get xkcd by number.  If no number is given, return the most \
                \up-to-date comic."
    , execute = getComicNr
    }

relevantXkcd :: CmdMap
relevantXkcd = mkCmd $ defCmd
    { usage   = "relevant-xkcd"
    , help    = "Get a completely relevant xkcd :>"
    , execute = \msg -> priv msg . urlWithAltText =<< randomMember =<< getXkcds
    }

-- | Get a comic by its number.
getComicNr :: Msg -> Net ()
getComicNr msg@Msg{ content } = do
    let n = fromMaybe 0 (tmaybeRead content)

    xkcds <- getXkcds
    let size = Set.size xkcds

    if n < 0 || n > size + 1
       then pass
       else priv msg . maybe mempty urlWithAltText $
              if n == 0
              then Set.lookupMax xkcds
              else Set.lookupMin . Set.filter ((== n) . num) $ xkcds
              -- Some comics may be missing (either as a joke, like 404,
              -- or because the connection failed), so we can't just get
              -- the index here.

-- | Worker function for filtering all comics according to some specifications.
getWith :: (Text -> Xkcd -> Bool) -> Msg -> Net ()
getWith matchOn msg@Msg{ content = query }
    | isEmpty query = getComicNr msg
    | otherwise
          = priv msg
          . maybe "Couldn't find anything with that name :(" urlWithAltText
          . Set.lookupMin
          . Set.filter (matchOn query)
        =<< getXkcds

-- | Download __all__ comics.
dlXkcds :: IO (Set Xkcd)
dlXkcds = mconcat <$> do
    man <- newManager tlsManagerSettings
    n   <- getNumber man

    -- Spawn a new thread for every connection because we're living in the fast
    -- lane over here.
    mapConcurrently (decodeComic man) [1 .. n]

-- | Get current number of xkcd comics.
getNumber :: MonadUnliftIO m => Manager -> m Int
getNumber man = makeConnection man 0 (maybe 0 num) 0

-- | Given a number of comics to query, query them!
decodeComic :: MonadUnliftIO m => Manager -> Int -> m (Set Xkcd)
decodeComic man n = makeConnection man n (maybe mempty one) mempty

-- | Connect to some database and try to get Xkcds.
makeConnection
    :: MonadUnliftIO m
    => Manager            -- ^ Manager for the connection
    -> Int                -- ^ Number of comic to get
    -> (Maybe Xkcd -> c)  -- ^ How to deal with decoding failures
    -> c                  -- ^ Default value for connection error
    -> m c
makeConnection man n mbA def = print n >> catch
    do request <- fromEither . parseUrlThrow  . toString . xkcdAddress $ n
       mbA . decode . responseBody <$> httpLbs request man
    \(_ :: SomeException) -> pure def

-- | Query a comic by number, where @0@ is the current comic.
xkcdAddress :: Int -> Text
xkcdAddress n =
    "https://xkcd.com/" <> (if n == 0 then "" else show n) <> "/info.0.json"

-- | Check for new comics on the XKCD website every now and then.
checkForNewComics :: Net ()
checkForNewComics = do
    man <- getManager

    -- Get latest comic and compare with the newest one we have.
    n <- getNumber man
    m <- maybe 0 num . Set.lookupMax <$> getXkcds
    when (n > m) do
        new <- mconcat <$> mapConcurrently (decodeComic man) [m .. n]
        modifyXkcds (<> new)

    threadDelay (1_000_000 * 60 * 60 * 24)  -- every 24 hours
    checkForNewComics

urlWithAltText :: Xkcd -> Text
urlWithAltText Xkcd{ img, alt } = img <> " (" <> alt <> ")"
