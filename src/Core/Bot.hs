{-# LANGUAGE DeriveAnyClass #-}
module Core.Bot
    ( -- * Custom monad stack the bot lives in
      Net             -- newtype wrapper: ReaderT Bot IO

      -- * Immutable and mutable bot state
    , Bot(..)
    , Command(Command, usage, prefix, help, enabled, execute, lastUsed, cooldown)
                      -- instances: Generic
    , Subs            -- type alias: HashMap Text (Set Text)
    , Options(..)     -- instances: Generic
    , Xkcd(..)        -- instances: Show, Eq, Ord, Generic, FromJSON

      -- * Type classes for modifying only that state
    , ConfigRead(..)
    , ConfigWrite(..)

    , MatchFun -- type alias: Command -> Text -> Bool
    , CmdMap   -- type alias: HashMap Command MatchFun

      -- * Manipulating the bot
    , emptyOpts       -- :: Options
    , defCmd          -- :: Command
    , mkCmd           -- :: Command -> CmdMap
    , mkConstraintCmd -- :: Command -> MatchFun -> CmdMap
    , matchCmd        -- :: MatchFun
    , runNetMonad     -- :: Net a -> BotConfig -> Bot -> IO a
    ) where

import Core.Message (Msg)

import Data.Text qualified as T
import Text.Show qualified

import Data.Aeson (FromJSON)
import Data.Generics.Labels ()
import Data.Time
    ( Day(ModifiedJulianDay)
    , NominalDiffTime
    , UTCTime(UTCTime)
    , secondsToNominalDiffTime
    )
import Network.HTTP.Conduit (Manager)
import Network.Socket (HostName, PortNumber)


-- | The holy thing itself.
data Bot = Bot
    { -- | Immutable state
      socket  :: !Handle
    , server  :: !HostName
    , port    :: !PortNumber
    , manager :: !Manager

      -- | Mutable state
    , nick      :: !(TVar Text)                    -- ^ Nickname of the bot
    , admins    :: !(TVar (Set Text))              -- ^ Set of all privileged users
    , ignores   :: !(TVar (Set Text))              -- ^ Set of all banned users
    , subs      :: !(TVar Subs)                    -- ^ All subs
    , allCmds   :: !(TVar CmdMap)                  -- ^ __All__ commands
    , chanOpts  :: !(TVar (HashMap Text Options))  -- ^ Options for a specific channel
    , xkcds     :: !(TVar (Set Xkcd))
    }

-- | Channel specific options.
data Options = Options
    { nicks :: !(Set Text)                -- ^ All people in an IRC channel.
    , tells :: !(HashMap Text (Set Msg))  -- ^ All tells for a channel.
    , cmds  :: !CmdMap                    -- ^ All commands users can execute.
    } deriving stock (Generic)

data Xkcd = Xkcd
    { num        :: !Int   -- ^ Number of comic.
    , title      :: !Text
    , img        :: !Text  -- ^ URL to the image
    , alt        :: !Text  -- ^ Hover text
    , transcript :: !Text
    }
    deriving stock    (Show, Generic)
    deriving anyclass (FromJSON)

-- | The only thing we (currently) care about for 'Eq' and 'Ord'
-- instances is the number of the comic; this should _hopefully_ be
-- unique.
instance Eq Xkcd where
    (==) :: Xkcd -> Xkcd -> Bool
    x == x' = num x == num x'

instance Ord Xkcd where
    (<=) :: Xkcd -> Xkcd -> Bool
    x <= x' = num x <= num x'

type CmdMap   = HashMap Command MatchFun
type MatchFun = Command -> Text -> Bool

-- | Type for a command that a user send to the bot.
data Command = Command
    { usage       :: !Text
      -- ^ Minimal usage example without a prefix; this is useful for
      -- 1) printing the command and 2) disambiguating commands.
    , prefix      :: !Text             -- ^ Prefix for triggering the command
    , help        :: !Text             -- ^ How to use the command
    , enabled     :: !Bool             -- ^ Whether execution of the command is allowed
    , execute     :: !(Msg -> Net ())  -- ^ How to execute the action
    , lastUsed    :: !UTCTime
    , cooldown    :: !NominalDiffTime  -- ^ In seconds
    }
    deriving stock    (Generic)

instance Hashable Command where
  hashWithSalt :: Int -> Command -> Int
  hashWithSalt i Command{ usage } = hashWithSalt i usage

-- | 'Eq' and 'Ord' instances, as this is later used in a map.
instance Eq Command where
    (==) :: Command -> Command -> Bool
    a == b = usage a == usage b
instance Ord Command where
    (<=) :: Command -> Command -> Bool
    a <= b = usage a <= usage b

-- | 'Show' for 'Command' can't be derived because we have some
instance Show Command where
    show :: Command -> String
    show Command{ prefix, usage, enabled, lastUsed, cooldown } = toList $
        mconcat [ "Command: ", prefix, usage
                , ", enabled: "  , show enabled
                , ", last used: ", show lastUsed
                , ", cooldown: " , show cooldown
                ]

defCmd :: Command
defCmd = Command
    { usage       = "I haven't been given a usage description!  \
                    \Please slap the person running the bot for me."
    , prefix      = ""
    , help        = "I haven't been given any help text!  \
                    \Please slap the person running the bot for me."
    , enabled     = True
    , execute     = const pass
    , lastUsed    = UTCTime (ModifiedJulianDay 0) 0
    , cooldown    = secondsToNominalDiffTime 4
    }

-- | Template for a default command.
mkCmd :: Command -> CmdMap
mkCmd = mkConstraintCmd matchCmd

-- | A command that specifies a custom constraint in order to be
-- executed.
mkConstraintCmd :: MatchFun -> Command -> CmdMap
mkConstraintCmd f c = one (c, f)

-- | The default way to match commands.
matchCmd :: MatchFun
matchCmd Command{prefix, usage} line =
    (prefix <> usage <> " ") `T.isPrefixOf` (line <> " ")
    -- Note that the two spaces are important here in order for commands
    -- that are proper subsets of other commands to also get noticed.

-- | The @Net@ monad, a newtype for a wrapper for a 'Reader' over 'IO',
-- carrying the bots immutable and mutable state.
type Net :: Type -> Type
newtype Net a = Net (ReaderT Bot IO a)
    deriving newtype
        ( Functor
        , Applicative
        , Monad
        , MonadIO         -- Net can do 'IO'
        , MonadUnliftIO   -- Allow action to be run in 'IO'
        , MonadReader Bot -- Avoid wrapping asks
        )

-- | Empty options.
emptyOpts :: Options
emptyOpts = Options mempty mempty mempty

-- | Type for a subscription that users may subscribe to, in order to get
-- highlighted for certain events.
type Subs = HashMap Text (Set Text)

-- | Monad for reading the bots internal state.
type ConfigRead :: (Type -> Type) -> Constraint
class MonadReader Bot m => ConfigRead m where
    getSocket    :: m Handle
    getServer    :: m HostName
    getPort      :: m PortNumber
    getManager   :: m Manager
    getNick      :: m Text
    getAdmins    :: m (Set Text)
    getIgnores   :: m (Set Text)
    getSubs      :: m Subs
    getAllCmds   :: m CmdMap
    getChanOpts  :: m (HashMap Text Options)
    getXkcds     :: m (Set Xkcd)

-- | Instance for the monad we will actually be using.
instance ConfigRead Net where
    getSocket   = asks socket
    getServer   = asks server
    getPort     = asks port
    getManager  = asks manager
    getNick     = readTVarIO =<< asks nick
    getAdmins   = readTVarIO =<< asks admins
    getIgnores  = readTVarIO =<< asks ignores
    getSubs     = readTVarIO =<< asks subs
    getAllCmds  = readTVarIO =<< asks allCmds
    getChanOpts = readTVarIO =<< asks chanOpts
    getXkcds    = readTVarIO =<< asks xkcds

-- | Monad for writing to the bots internal state.
type ConfigWrite :: (Type -> Type) -> Constraint
class MonadReader Bot m => ConfigWrite m where
    modifyNick    :: (Text     -> Text)     -> m ()
    modifyAdmins  :: (Set Text -> Set Text) -> m ()
    modifyIgnores :: (Set Text -> Set Text) -> m ()
    modifySubs    :: (Subs     -> Subs)     -> m ()
    modifyXkcds   :: (Set Xkcd -> Set Xkcd) -> m ()
    modifyOpts    :: (HashMap Text Options -> HashMap Text Options) -> m ()

-- | Regain some purity by at least not using IO everywhere I use the 'Net'
-- monad.
instance ConfigWrite Net where
    modifyNick    = (asks nick     >>=) . (atomically .: flip modifyTVar')
    modifyAdmins  = (asks admins   >>=) . (atomically .: flip modifyTVar')
    modifyIgnores = (asks ignores  >>=) . (atomically .: flip modifyTVar')
    modifySubs    = (asks subs     >>=) . (atomically .: flip modifyTVar')
    modifyXkcds   = (asks xkcds    >>=) . (atomically .: flip modifyTVar')
    modifyOpts    = (asks chanOpts >>=) . (atomically .: flip modifyTVar')

-- | Running the Net monad.
runNetMonad :: Net a -> Bot -> IO a
runNetMonad = runReaderT . un
