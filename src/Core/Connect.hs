module Core.Connect
    ( connectTo  -- :: N.HostName -> N.PortNumber -> IO Handle
    ) where

import Network.Socket qualified as N


-- | Connect to a server given its name and port number (helper for connect)
connectTo :: N.HostName -> N.PortNumber -> IO Handle
connectTo host port = do
    addr : _ <- N.getAddrInfo Nothing (Just host) (Just (show port))
    sock <- N.socket (N.addrFamily addr) (N.addrSocketType addr) (N.addrProtocol addr)
    N.connect sock (N.addrAddress addr)
    N.socketToHandle sock ReadWriteMode
