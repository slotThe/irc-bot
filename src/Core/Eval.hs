module Core.Eval
    ( eval  -- :: Msg -> Net ()
    ) where

import Core.Bot (Command (Command, cooldown, enabled, execute, lastUsed), ConfigRead (getAllCmds, getChanOpts), Net, modifyOpts)
import Core.Message (Msg (Msg, admin, channel, content))
import Core.Util (getMatched, getMatchedAdd)

import Data.HashMap.Strict qualified as Map
import Data.Text           qualified as T

import Data.Time (diffUTCTime, getCurrentTime)


-- | Dispatch a command if something matched the input.
eval :: Msg -> Net ()
eval msg@Msg{ content, channel, admin }
  | "#" `T.isInfixOf` channel =
      whenJustM (getMatched channel content) executeAndUpdate
  | otherwise = do
      allCmds <- getAllCmds
      whenJustM (getMatchedAdd allCmds channel content) (`execute` msg')
 where
  -- | Assuming a command was found, see if it's enabled and the specific
  -- cooldown is over.  If yes, execute it.
  executeAndUpdate :: Command -> Net ()
  executeAndUpdate cmd@Command{ enabled, cooldown, lastUsed } = do
    time <- liftIO getCurrentTime
    let cooldownOver = diffUTCTime time lastUsed > cooldown

    when (enabled && (cooldownOver || admin)) $ do
      execute cmd msg'
      whenM ((channel `Map.member`) <$> getChanOpts) $
        modifyOpts $ Map.adjust (#cmds %~ modifyKey cmd (#lastUsed .~ time)) channel

  -- | Remove the command from the message and strip it of any spaces.
  msg' :: Msg
  msg' = msg & #content %~ T.strip . dropWhile1 (/= ' ')
