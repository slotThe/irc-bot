module Core.ListenAndProcess
    ( joinAndListen  -- :: Net
    ) where

import Core.State qualified     as Cmd
import Cmds.WriteFile qualified as Cmd
import Cmds.Xkcd qualified      as Xkcd

import Relude.Unsafe qualified as Unsafe

import Cmds.UserTell (checkTells)
import Core.Bot (Bot (nick, socket), ConfigRead (getAdmins, getChanOpts, getIgnores, getNick), ConfigWrite, Net, execute, modifyOpts)
import Core.Eval (eval)
import Core.Message (Msg (Msg, admin, banned, botNick, channel, command, content, sender))
import Core.Parser.IrcMessage.Extract (extractMsg)
import Core.Parser.IrcMessage.Parser (pIrcMessage)
import Core.Parser.IrcMessage.Types (IrcCommand (ERR_NICKNAMEINUSE, JOIN, NICK, PART, PING, PONG, PRIVMSG, QUIT, RPL_NAMREPLY, USER), IrcMessage)
import Core.State (addName, altBotName, removeName, removeNameEverywhere, updateName)
import Core.Write (privmsg, write)

import Data.HashMap.Strict qualified as Map
import Data.Text qualified           as T
import Data.Text.IO qualified        as T

import Text.Megaparsec (runParser)
import UnliftIO.Concurrent (forkIO)


-- | We're in the Net monad now, so we've connected successfully.
-- Join a channel, and start processing commands.
joinAndListen :: Net ()
joinAndListen = do
  chans <- Map.keys <$> getChanOpts

  -- Set will at most contain one element at this point.
  let chan = fromMaybe "" (listToMaybe chans)

  nick <- getNick
  write NICK   nick
  write USER $ nick <> " 0 * :yo guy!"
  write JOIN   chan  -- FIXME: This does not make the bot join a channel on
                     -- quakenet but works on libera...

  -- Write irc messages as the bot.
  void . forkIO $ interactivePrompt

  -- Check for new XKCDs every now and then.
  void . forkIO $ Xkcd.checkForNewComics

  -- Listen to incoming messages synchronously.
  listen

-- | Extremely basic interfacing for sending irc messages as the bot.
-- TODO Make this less painful.
interactivePrompt :: Net ()
interactivePrompt = forever do
  text <- getLine
  let (h, t) = theadAndTail text
  case tmaybeRead h of
    Nothing -> privmsg h t
    Just c  -> do
      bNick <- readTVarIO =<< asks nick
      let (chan, _) = theadAndTail t
      let msg = Msg
            { admin   = True
            , banned  = False
            , botNick = bNick
            , channel = chan
            , command = c
            , content = chan
            , sender  = bNick
            }
      decideInput msg c
 where
  theadAndTail :: Text -> (Text, Text)
  theadAndTail = liftA2 (,) (T.takeWhile (/= ' ')) (dropWhile1 (/=' '))

  decideInput :: Msg -> IrcCommand -> Net ()
  decideInput msg = \case
    JOIN -> execute (fst $ Unsafe.head $ toList Cmd.joinChan     ) msg
    QUIT -> execute (fst $ Unsafe.head $ toList Cmd.quit         ) msg
    NICK -> execute (fst $ Unsafe.head $ toList Cmd.changeBotNick) msg
    _    -> pass -- Ignore the rest.

-- | Process each line from the server
listen :: Net ()
listen = forever do
  h <- asks socket

  l <- liftIO $ T.hGetLine h  -- Get line from server.
  putTextLn l                 -- Print that line to console.
  let line = T.init l         -- Discard last element (newline char).

  -- Actually parse the IRC message and process the output.
  case runParser pIrcMessage "" line of
    Left _ -> pass  -- Silently fail.

    -- Build the message, then decide what to actually do with it.
    Right ircMsg -> processLine =<< makeMsg ircMsg

-- | Given an 'IrcMessage', get some values from the bots environment and then
-- create a message based upon the parsed data.
makeMsg :: ConfigRead m => IrcMessage -> m Msg
makeMsg ircMsg = extractMsg ircMsg <$> getNick <*> getAdmins <*> getIgnores

{- | Match on IRC code and then execute the specified action.

   This is mainly for things that need either the output of a certain command to
   work or get the name of the users who asked something.
-}
processLine :: Msg -> Net ()
processLine msg@Msg{ content, sender, command, channel } =
  case command of
    PRIVMSG           -> decideEval    msg'
    PING              -> pong          content
    ERR_NICKNAMEINUSE -> altBotName
    PART              -> removeName    sender channel
    QUIT              -> removeNameEverywhere sender
    NICK              -> updateName    msg
    JOIN              -> addCheckTells msg
    RPL_NAMREPLY      -> addNames      msg
    _                 -> pass  -- Ignore the rest.
 where
  -- | Answer to ping message, required by some networks to stay connected.
  pong :: Text -> Net ()
  pong s = write PONG (':' `T.cons` s)

  msg' :: Msg
  msg' = msg & #content %~ T.strip

{- | Add a name to the set of names, check if there's any tells for that user.
   This is called when a new user joins the channel, so `sender` is exactly
   what we want here.
-}
addCheckTells :: Msg -> Net ()
addCheckTells msg@Msg{ sender, channel } =
  addName sender channel *> checkTells msg

-- | Add all people of names output to set of names.
addNames :: (ConfigWrite m, ConfigRead m) => Msg -> m ()
addNames Msg{ content, channel } = do
  opts <- getChanOpts
  when (channel `Map.member` opts) $
    modifyOpts $ Map.adjust (#nicks <>~ filterOPandVoice content) channel
 where
  filterOPandVoice :: Text -> Set Text
  filterOPandVoice = fromList . words . T.filter (/= '+') . T.filter (/= '@')
{-# SPECIALIZE addNames :: Msg -> Net () #-}

-- | Decide whether to bother evaluating the command.
decideEval :: Msg -> Net ()
decideEval msg@Msg{ content, banned }
  | banned || isEmpty content = pass
  | otherwise                 = eval msg
