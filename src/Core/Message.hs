module Core.Message
    ( Msg(..)  -- instances: Eq, Ord, Show, Generic
    ) where

import Core.Parser.IrcMessage.Types (IrcCommand)
import Data.Generics.Labels ()


-- | Type for an IRC message.
data Msg = Msg
    { admin   :: !Bool        -- ^ Admin?
    , banned  :: !Bool        -- ^ Allowed to send messages?
    , botNick :: !Text        -- ^ Current nick of the bot.
    , channel :: !Text        -- ^ Channel in which the sender resides.
    , command :: !IrcCommand  -- ^ What type of message is it?
    , content :: !Text        -- ^ Command the user send to the bot.
    , sender  :: !Text        -- ^ Who send the message?
    } deriving stock (Eq, Ord, Show, Generic)
