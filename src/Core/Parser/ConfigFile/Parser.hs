module Core.Parser.ConfigFile.Parser
    ( ConfigFile(..)  -- instances: Show
    , pConfigFile     -- :: Parser ConfigFile
    , startAdmins     -- :: Text
    , startBans       -- :: Text
    , startChan       -- :: Text
    , startNick       -- :: Text
    , startSubs       -- :: Text
    ) where

import Core.Parser.Util (Parser, neitherOf, takeText, takeWhile1)

import Control.Applicative.Permutations
    ( Permutation
    , runPermutation
    , toPermutation
    , toPermutationWithDefault
    )
import Text.Megaparsec (ParsecT, sepBy)
import Text.Megaparsec.Char
    ( char
    , newline
    , newline
    , space
    , space1
    , string
    , symbolChar
    )


-- | Config type for all things the bot should probably "remember".
data ConfigFile = Config
    { initNick :: !Text
    , initChan :: !(Maybe Text)
    , admins   :: !(Maybe (Set Text))
    , bans     :: !(Maybe (Set Text))
    , subs     :: !(Maybe [(Text, Set Text)])
    , prefix   :: !(Maybe Text)
    } deriving stock (Show)

-- | Parse a config file.
pConfigFile :: Parser ConfigFile
pConfigFile = runPermutation $
    Config <$> toPermutation (pNick  <* many newline)
           <*> defPerm pChan
           <*> defPerm pAdmins
           <*> defPerm pBans
           <*> defPerm pSubs
           <*> defPerm pPrefix
  where
    defPerm :: Parser a -> Permutation (ParsecT Void Text Identity) (Maybe a)
    defPerm = toPermutationWithDefault Nothing . (Just <$>) . (<* many newline)

-- | Parse at least one sub.
pSubs :: Parser [(Text, Set Text)]
pSubs = string startSubs >> some pSub

-- | Parse a single sub.
pSub :: Parser (Text, Set Text)
pSub = (,) <$> (space1 *> takeWhile1 (/= ':'))
           <*> ((char ':' *> space) *> pList <* newline)

-- | Parse a nick.
pNick :: Parser Text
pNick = withSpaces startNick *> takeText

-- | Parse an IRC channel.
pChan :: Parser Text
pChan = withSpaces startChan *> takeText

-- | Parse a list of admins.
pAdmins :: Parser (Set Text)
pAdmins = withSpaces startAdmins *> pList

-- | Parse a list of bans.
pBans :: Parser (Set Text)
pBans = withSpaces startBans *> pList

-- | Parse a prefix.
pPrefix :: Parser Text
pPrefix = one <$> (withSpaces startPrefix *> symbolChar)

-- | Parse a list of space separated names and turn them into a set.
pList :: Parser (Set Text)
pList = fromList <$> takeWhile1 (neitherOf ' ' '\n') `sepBy` char ' '

-- | A starting word, following by at least one space.
withSpaces :: Text -> Parser ()
withSpaces word = string word *> space1

-- | How a nick declaration will start in the config.
startNick :: Text
startNick = "nick:"

-- | How a chan declaration will start in the config.
startChan :: Text
startChan = "chan:"

-- | How a list of admins will start in the config.
startAdmins :: Text
startAdmins = "admins:"

-- | How a list of bans will start in the config.
startBans :: Text
startBans = "bans:"

-- | How a list of subs will start in the config.
startSubs :: Text
startSubs = "subs:"

startPrefix :: Text
startPrefix = "prefix:"
