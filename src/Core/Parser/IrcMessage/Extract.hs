module Core.Parser.IrcMessage.Extract
    ( extractMsg  -- :: IrcMessage -> Text -> Set Text -> Set Text -> Msg
    ) where

import Core.Message (
    Msg (Msg, admin, banned, botNick, channel, command, content, sender),
 )
import Core.Parser.IrcMessage.Types (
    IrcMessage (IrcMessage, command, params, prefix),
    Params,
    Params' (Middle, Trailing),
    Prefix (User),
 )

import Data.Set qualified  as Set
import Data.Text qualified as T


-- | Build a 'Msg' based upon a parsed IRC message and all the other necessary
-- components.
extractMsg :: IrcMessage -> Text -> Set Text -> Set Text -> Msg
extractMsg ircMsg@IrcMessage{ command } botNick privs bans =
    Msg { admin   = senderHost `Set.member` privs
        , banned  = senderHost `Set.member` bans
        , botNick = botNick
        , channel = senderChannel sender $ extractChannel ircMsg
        , content = extractTrailing ircMsg
        , sender  = sender
        , command = command
        }
  where
    user :: (Text, Text, Text)
    user = extractUser ircMsg

    senderHost, sender :: Text
    senderHost = thrd3 user
    sender     = fst3  user

{- | Gets the proper channel from a privmsg.
   This is needed as I need the sender nick for a pm, not the nick of the bot
   (where they send the message to).
-}
senderChannel :: Text -> Text -> Text
senderChannel senderNick channel
    | "#" `T.isPrefixOf` channel = channel
    | otherwise                  = senderNick
      -- No proper channel -> must have been a pm.

-- | Get an IRC user out of a parsed message.
extractUser :: IrcMessage -> (Text, Text, Text)  -- ^ (Nick, Name, Host)
extractUser IrcMessage{ prefix } = case prefix of
    Nothing                    -> ("", "", "")
    Just (User nick name host) -> (nick, extract name, extract host)
  where
    extract :: Maybe Text -> Text
    extract = fromMaybe ""

-- | Extract some parameter out of an IRC message.
extractParam :: (Params -> Text) -> IrcMessage -> Text
extractParam f IrcMessage{ params }
   | null params = ""
   | otherwise   = f params

-- | Extract a 'Trailing' message from the prefix.  This will be (for the most
-- part) the message that a user actually send to the channel/bot.
extractTrailing :: IrcMessage -> Text
extractTrailing = extractParam getTrailing

-- | Get the channel a message was send to.
extractChannel :: IrcMessage -> Text
extractChannel = extractParam getLastMiddle

-- | Look for the 'Trailing' data constructor.
getTrailing :: Params -> Text
getTrailing = \case
    []     ->""
    (p:ps) -> case p of
        Trailing t -> t
        _          -> getTrailing ps

-- | Look for the last middle prefix.  This will be the channel the message was
-- send to.
getLastMiddle :: Params -> Text
getLastMiddle = \case
    []           -> ""
    [Middle   m] -> m
    [Trailing _] -> ""
    (_:ps)       -> getLastMiddle ps
