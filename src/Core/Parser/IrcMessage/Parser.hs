module Core.Parser.IrcMessage.Parser
    ( pIrcMessage  -- :: Parser IrcMessage
    ) where

import Core.Parser.IrcMessage.Types
    ( IrcCommand(..)
    , IrcMessage(IrcMessage)
    , Params
    , Params'(Middle, Trailing)
    , Prefix(User)
    )
import Core.Parser.Util (Parser, neitherOf, takeText, takeWhile1)

import Text.Megaparsec (between, choice, sepEndBy)
import Text.Megaparsec.Char (char, string)


{- 'pseudo' BNF for an IRC message:

   <message>  ::= [':' <prefix> <SPACE> ] <command> <params> <crlf>
   <prefix>   ::= <servername> | <nick> [ '!' <user> ] [ '@' <host> ]
   <command>  ::= <letter> { <letter> } | <number> <number> <number>
   <SPACE>    ::= ' ' { ' ' }
   <params>   ::= <SPACE> [ ':' <trailing> | <middle> <params> ]

   <middle>   ::= <Any *non-empty* sequence of octets not including SPACE
                  or NUL or CR or LF, the first of which may not be ':'>
   <trailing> ::= <Any, possibly *empty*, sequence of octets not including
                    NUL or CR or LF>

   <crlf>     ::= CR LF

   NOTE: I cheated a bit in some parts (i.e. I only parse a single space for
         <SPACE> instead of a positive natural number of spaces), let's see if
         this comes back to bite me at some point :>
-}

-- | Parse a trailing parameter.
pTrailing :: Parser Params'
pTrailing = char ':' *> (Trailing <$> takeText)

-- | Parse a middle parameter.
pMiddle :: Parser Params'
pMiddle = Middle <$> takeWhile1 (neitherOf ' ' ':')

-- | Parse a parameter.
pParams :: Parser Params
pParams = do
    mid <- char ' ' *> (pMiddle `sepEndBy` char ' ')
    optional pTrailing <&> \case
        Just t  -> t : mid  -- Trailing gets inserted at the front!
        Nothing -> mid

-- | Parse an IRC prefix.
pPrefix :: Parser Prefix
pPrefix =
    User <$> takeWhile1 (neitherOf '!' ' ')
         <*> optional (char '!' *> takeWhile1 (neitherOf '@' ' '))
         <*> optional (char '@' *> takeWhile1 (neitherOf ' ' ' '))

-- | Parse a whole IRC message.
pIrcMessage :: Parser IrcMessage
pIrcMessage =
    IrcMessage <$> optional (between (char ':') (char ' ') pPrefix)
               <*> pCommand
               <*> pParams

-- | Parse an IRC command.
-- Source: http://www.networksorcery.com/enp/protocol/irc.htm
pCommand :: Parser IrcCommand
pCommand = choice
    [ PRIVMSG              <$ string "PRIVMSG"
    , NICK                 <$ string "NICK"
    , JOIN                 <$ string "JOIN"
    , PART                 <$ string "PART"
    , QUIT                 <$ string "QUIT"
    , PING                 <$ string "PING"
    , NOTICE               <$ string "NOTICE"
    , MODE                 <$ string "MODE"
    , ADMIN                <$ string "ADMIN"
    , AWAY                 <$ string "AWAY"
    , CONNECT              <$ string "CONNECT"
    , DIE                  <$ string "DIE"
    , ERROR                <$ string "ERROR"
    , INFO                 <$ string "INFO"
    , INVITE               <$ string "INVITE"
    , ISON                 <$ string "ISON"
    , KICK                 <$ string "KICK"
    , KILL                 <$ string "KILL"
    , LINKS                <$ string "LINKS"
    , LIST                 <$ string "LIST"
    , LUSERS               <$ string "LUSERS"
    , MOTD                 <$ string "MOTD"
    , NAMES                <$ string "NAMES"
    , NJOIN                <$ string "NJOIN"
    , OPER                 <$ string "OPER"
    , PASS                 <$ string "PASS"
    , PONG                 <$ string "PONG"
    , REHASH               <$ string "REHASH"
    , RESTART              <$ string "RESTART"
    , SERVER               <$ string "SERVER"
    , SERVICE              <$ string "SERVICE"
    , SERVLIST             <$ string "SERVLIST"
    , SQUERY               <$ string "SQUERY"
    , SQUIRT               <$ string "SQUIRT"
    , SQUIT                <$ string "SQUIT"
    , STATS                <$ string "STATS"
    , SUMMON               <$ string "SUMMON"
    , TIME                 <$ string "TIME"
    , TOPIC                <$ string "TOPIC"
    , TRACE                <$ string "TRACE"
    , USER                 <$ string "USER"
    , USERHOST             <$ string "USERHOST"
    , USERS                <$ string "USERS"
    , VERSION              <$ string "VERSION"
    , WALLOPS              <$ string "WALLOPS"
    , WHO                  <$ string "WHO"
    , WHOIS                <$ string "WHOIS"
    , WHOWAS               <$ string "WHOWAS"
    , Welcome1             <$ string "001"
    , Welcome2             <$ string "002"
    , Welcome3             <$ string "003"
    , Welcome4             <$ string "004"
    , Welcome5             <$ string "005"
    , RPL_TRACELINK        <$ string "200"
    , RPL_TRACECONNECTING  <$ string "201"
    , RPL_TRACEHANDSHAKE   <$ string "202"
    , RPL_TRACEUNKNOWN     <$ string "203"
    , RPL_TRACEOPERATOR    <$ string "204"
    , RPL_TRACEUSER        <$ string "205"
    , RPL_TRACESERVER      <$ string "206"
    , RPL_TRACENEWTYPE     <$ string "208"
    , RPL_TRACELOG         <$ string "261"
    , RPL_STATSLINKINFO    <$ string "211"
    , RPL_STATSCOMMANDS    <$ string "212"
    , RPL_STATSCLINE       <$ string "213"
    , RPL_STATSNLINE       <$ string "214"
    , RPL_STATSILINE       <$ string "215"
    , RPL_STATSKLINE       <$ string "216"
    , RPL_STATSYLINE       <$ string "218"
    , RPL_ENDOFSTATS       <$ string "219"
    , RPL_STATSLLINE       <$ string "241"
    , RPL_STATSUPTIME      <$ string "242"
    , RPL_STATSOLINE       <$ string "243"
    , RPL_STATSHLINE       <$ string "244"
    , RPL_UMODEIS          <$ string "221"
    , RPL_LUSERCLIENT      <$ string "251"
    , RPL_LUSEROP          <$ string "252"
    , RPL_LUSERUNKNOWN     <$ string "253"
    , RPL_LUSERCHANNELS    <$ string "254"
    , RPL_LUSERME          <$ string "255"
    , RPL_ADMINME          <$ string "256"
    , RPL_ADMINLOC1        <$ string "257"
    , RPL_ADMINLOC2        <$ string "258"
    , RPL_ADMINEMAIL       <$ string "259"
    , RPL_NONE             <$ string "300"
    , RPL_USERHOST         <$ string "302"
    , RPL_ISON             <$ string "303"
    , RPL_AWAY             <$ string "301"
    , RPL_UNAWAY           <$ string "305"
    , RPL_NOWAWAY          <$ string "306"
    , RPL_WHOISUSER        <$ string "311"
    , RPL_WHOISSERVER      <$ string "312"
    , RPL_WHOISOPERATOR    <$ string "313"
    , RPL_WHOISIDLE        <$ string "317"
    , RPL_ENDOFWHOIS       <$ string "318"
    , RPL_WHOISCHANNELS    <$ string "319"
    , RPL_WHOWASUSER       <$ string "314"
    , RPL_ENDOFWHOWAS      <$ string "369"
    , RPL_LISTSTART        <$ string "321"
    , RPL_LIST             <$ string "322"
    , RPL_LISTEND          <$ string "323"
    , RPL_CHANNELMODEIS    <$ string "324"
    , RPL_NOTOPIC          <$ string "331"
    , RPL_TOPIC            <$ string "332"
    , RPL_INVITING         <$ string "341"
    , RPL_SUMMONING        <$ string "342"
    , RPL_VERSION          <$ string "351"
    , RPL_WHOREPLY         <$ string "352"
    , RPL_ENDOFWHO         <$ string "315"
    , RPL_NAMREPLY         <$ string "353"
    , RPL_ENDOFNAMES       <$ string "366"
    , RPL_LINKS            <$ string "364"
    , RPL_ENDOFLINKS       <$ string "365"
    , RPL_BANLIST          <$ string "367"
    , RPL_ENDOFBANLIST     <$ string "368"
    , RPL_INFO             <$ string "371"
    , RPL_ENDOFINFO        <$ string "374"
    , RPL_MOTDSTART        <$ string "375"
    , RPL_MOTD             <$ string "372"
    , RPL_ENDOFMOTD        <$ string "376"
    , RPL_YOUREOPER        <$ string "381"
    , RPL_REHASHING        <$ string "382"
    , RPL_TIME             <$ string "391"
    , RPL_USERSSTART       <$ string "392"
    , RPL_USERS            <$ string "393"
    , RPL_ENDOFUSERS       <$ string "394"
    , RPL_NOUSERS          <$ string "395"
    , ERR_NOSUCHNICK       <$ string "401"
    , ERR_NOSUCHSERVER     <$ string "402"
    , ERR_NOSUCHCHANNEL    <$ string "403"
    , ERR_CANNOTSENDTOCHAN <$ string "404"
    , ERR_TOOMANYCHANNELS  <$ string "405"
    , ERR_WASNOSUCHNICK    <$ string "406"
    , ERR_TOOMANYTARGETS   <$ string "407"
    , ERR_NOORIGIN         <$ string "409"
    , ERR_NORECIPIENT      <$ string "411"
    , ERR_NOTEXTTOSEND     <$ string "412"
    , ERR_NOTOPLEVEL       <$ string "413"
    , ERR_WILDTOPLEVEL     <$ string "414"
    , ERR_UNKNOWNCOMMAND   <$ string "421"
    , ERR_NOMOTD           <$ string "422"
    , ERR_NOADMININFO      <$ string "423"
    , ERR_FILEERROR        <$ string "424"
    , ERR_NONICKNAMEGIVEN  <$ string "431"
    , ERR_ERRONEUSNICKNAME <$ string "432"
    , ERR_NICKNAMEINUSE    <$ string "433"
    , ERR_NICKCOLLISION    <$ string "436"
    , ERR_USERNOTINCHANNEL <$ string "441"
    , ERR_NOTONCHANNEL     <$ string "442"
    , ERR_USERONCHANNEL    <$ string "443"
    , ERR_NOLOGIN          <$ string "444"
    , ERR_SUMMONDISABLED   <$ string "445"
    , ERR_USERSDISABLED    <$ string "446"
    , ERR_NOTREGISTERED    <$ string "451"
    , ERR_NEEDMOREPARAMS   <$ string "461"
    , ERR_ALREADYREGISTRED <$ string "462"
    , ERR_NOPERMFORHOST    <$ string "463"
    , ERR_PASSWDMISMATCH   <$ string "464"
    , ERR_YOUREBANNEDCREEP <$ string "465"
    , ERR_KEYSET           <$ string "467"
    , ERR_CHANNELISFULL    <$ string "471"
    , ERR_UNKNOWNMODE      <$ string "472"
    , ERR_INVITEONLYCHAN   <$ string "473"
    , ERR_BANNEDFROMCHAN   <$ string "474"
    , ERR_BADCHANNELKEY    <$ string "475"
    , ERR_NOPRIVILEGES     <$ string "481"
    , ERR_CHANOPRIVSNEEDED <$ string "482"
    , ERR_CANTKILLSERVER   <$ string "483"
    , ERR_NOOPERHOST       <$ string "491"
    , ERR_UMODEUNKNOWNFLAG <$ string "501"
    , ERR_USERSDONTMATCH   <$ string "502"
    ]
