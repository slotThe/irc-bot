module Core.Parser.Util
    ( Parser      -- type alias: Parsec Void Text
    , takeWhile1  -- :: (Char -> Bool) -> Parser Text
    , takeText    -- :: Parser Text
    , neitherOf   -- :: Eq a => a -> a -> a -> Bool
    ) where

import Text.Megaparsec (Parsec, takeWhile1P)


-- | Simple parser type.
type Parser a = Parsec Void Text a

-- | takeWhile1P without a default case.
takeWhile1 :: (Char -> Bool) -> Parser Text
takeWhile1 = takeWhile1P Nothing

-- | Consume the input until the end of the line.
takeText :: Parser Text
takeText = takeWhile1P Nothing (/= '\n')

-- | Neither of the two.
neitherOf :: Eq a => a -> a -> a -> Bool
neitherOf c c' = (&&) <$> (/= c) <*> (/= c')
