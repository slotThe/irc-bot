module Core.State
  ( -- * Directly modifying the bots state
    addName               -- :: (ConfigRead m, ConfigWrite m) => Text -> Text -> m ()
  , removeName            -- :: (ConfigRead m, ConfigWrite m) => Text -> Text -> m ()
  , removeNameEverywhere  -- :: (ConfigRead m, ConfigWrite m) => Text -> m ()
  , updateName            -- :: Msg -> Net ()
  , altBotName            -- :: Net ()
  , runQuit               -- :: Net ()

    -- * Commands
  , joinChan              -- :: CmdMap
  , part                  -- :: CmdMap
  , addAdmin              -- :: CmdMap
  , ignore                -- :: CmdMap
  , quitNW                -- :: CmdMap
  , changeBotNick         -- :: CmdMap
  , unignore              -- :: CmdMap
  , removeAdmin           -- :: CmdMap
  , changePrefix          -- :: CmdMap
  , load                  -- :: CmdMap
  , unload                -- :: CmdMap
  ) where

import Cmds.Subscriptions.Sub (changeNameInSub)
import Cmds.UserTell (checkTells)
import Core.Bot (CmdMap, Command (execute, help, prefix, usage), ConfigRead (getAllCmds, getChanOpts), ConfigWrite (modifyAdmins, modifyIgnores, modifyNick, modifyOpts, modifySubs), Net, Options (Options, nicks), defCmd, emptyOpts, getNick, mkCmd)
import Core.Message (Msg (Msg, channel, content, sender), botNick)
import Core.Parser.IrcMessage.Types (IrcCommand (JOIN, NICK, PART, QUIT))
import Core.Util (getMatched, whenAdmin)
import Core.Write (write)

import Data.HashMap.Strict qualified as Map
import Data.Set qualified            as Set
import Data.Text qualified           as T


-- | Command for letting the bot join a channel.
joinChan :: CmdMap
joinChan = mkCmd $ defCmd
  { usage   = "join"
  , help    = "Make the bot join a channel.  Admin command."
  , execute = whenAdmin <*> runJoin
  }
 where
  -- | Join a new channel and update the bots config.  The @content@
  -- field contains the channel we would like to join and possibly a
  -- password.
  runJoin :: Msg -> Net ()
  runJoin Msg{ content = chan } = do
    allCmds <- getAllCmds
    modifyOpts $ Map.insert (stripPassword chan) (emptyOpts & #cmds .~ allCmds)
    write JOIN chan
   where
    -- | Don't include the password in the internal channel name.
    stripPassword :: Text -> Text
    stripPassword = T.takeWhile (/= ' ')

-- | Let the bot leave a channel.
part :: CmdMap
part = mkCmd $ defCmd
  { usage   = "part"
  , help    = "Make the bot leave a channel.  Admin command."
  , execute = whenAdmin <*> runPart
  }
 where
  -- | Leave a channel and update the bots config.
  -- `content` contains the channel we would like to leave.
  runPart :: Msg -> Net ()
  runPart Msg{ content = chan } = modifyOpts (Map.delete chan) *> write PART chan

-- | Command for adding an admin.
addAdmin :: CmdMap
addAdmin = mkCmd $ defCmd
  { usage   = "addAdmin"
  , help    = "Add a new admin to the bots data base.  Admin command."
  , execute = whenAdmin <*> runAddAdmin
  }
 where
  -- | Add an admin; @content@ corresponds to the hostname of the user we
  -- would like to promote.
  runAddAdmin :: ConfigWrite m => Msg -> m ()
  runAddAdmin Msg{ content = userHost } = modifyAdmins (Set.insert userHost)

-- | Command for removing an admin.
removeAdmin :: CmdMap
removeAdmin = mkCmd $ defCmd
  { usage   = "removeAdmin"
  , help    = "Remove a new admin from the bots data base.  Admin command."
  , execute = whenAdmin <*> runRemoveAdmin
  }
 where
  -- | Remove an admin; @content@ corresponds to the hostname of the user
  -- we would like to demote.
  runRemoveAdmin :: ConfigWrite m => Msg -> m ()
  runRemoveAdmin Msg{ content = userHost } = modifyAdmins (Set.delete userHost)

-- | Command for making the bot ignore someone.
ignore :: CmdMap
ignore = mkCmd $ defCmd
  { usage   = "ignore"
  , help    = "Make the bot ignore someone.  Admin command."
  , execute = whenAdmin <*> runIgnore
  }
 where
  -- | Make the bot ignore someone.  In this case, `content` corresponds
  -- to the hostname of the person we would like to ignore.
  runIgnore :: ConfigWrite m => Msg -> m ()
  runIgnore Msg{ content = userHost } = modifyIgnores (Set.insert userHost)

-- | Command for removing an ignore.
unignore :: CmdMap
unignore = mkCmd $ defCmd
  { usage   = "unignore"
  , help    = "Remove an ignore.  Admin command."
  , execute = whenAdmin <*> runRemoveIgnore
  }
 where
  -- | Remove an ignore; in this case, @content@ corresponds to the
  -- hostname of the person we would like to unignore.
  runRemoveIgnore :: ConfigWrite m => Msg -> m ()
  runRemoveIgnore Msg{ content = userHost } = modifyIgnores (Set.delete userHost)

-- | Command for changing the bots nick.
changeBotNick :: CmdMap
changeBotNick = mkCmd $ defCmd
  { usage   = "change-nick"
  , help    = "Change the nick of the bot.  Admin command."
  , execute = whenAdmin <*> runChangeBotNick
  }
 where
  -- | Change the nick of the bot, update it on IRC; in this case,
  -- @content@ corresponds to the new nick of the bot.
  runChangeBotNick :: Msg -> Net ()
  runChangeBotNick msg@Msg{ content = T.take 15 -> newNick, botNick } = do
    modifyNick (const newNick)
    write NICK newNick
    -- Update all commands that use the bots name as a prefix.
    runChangePrefix msg{ content = botNick <> " " <> newNick }

-- | Command for quitting IRC without writing any state to a file.
quitNW :: CmdMap
quitNW = mkCmd $ defCmd
  { usage   = "quitNW"
  , help    = "Make the bot quit IRC.  Admin command."
  , execute = (`whenAdmin` runQuit)
  }

-- | Quit IRC.
runQuit :: Net ()
runQuit = write QUIT ":The bot commited sudoku" *> exitSuccess

-- | Change the name of the bot, update it on IRC.
altBotName :: Net ()
altBotName = do modifyNick (<> "_")
                write NICK =<< getNick

-- | Add a new nick to the set of nicks (new user joined the channel).
addName :: (ConfigRead m, ConfigWrite m) => Text -> Text -> m ()
addName = updateNickWith Set.insert
{-# SPECIALIZE addName :: Text -> Text -> Net () #-}

-- | Remove a nick from the set of nicks (user left the channel).
removeName :: (ConfigRead m, ConfigWrite m) => Text -> Text -> m ()
removeName = updateNickWith Set.delete
{-# SPECIALIZE removeName :: Text -> Text -> Net () #-}

-- | Remove a nick from the set of nicks in all channels (user quit the
-- irc server).
removeNameEverywhere :: (ConfigRead m, ConfigWrite m) => Text -> m ()
removeNameEverywhere name =
    traverse_ (removeName name) . Map.keys =<< getChanOpts
{-# SPECIALISE removeNameEverywhere :: Text -> Net () #-}

-- | Generic function to update names.
updateNickWith
    :: (ConfigRead m, ConfigWrite m)
    => (Text -> Set Text -> Set Text)
    -> Text
    -> Text
    -> m ()
updateNickWith f name chan = do
    opts <- getChanOpts
    when (chan `Map.member` opts) $
        modifyOpts $ Map.adjust (#nicks %~ f name) chan
{-# SPECIALISE updateNickWith :: (Text -> Set Text -> Set Text) -> Text -> Text -> Net () #-}

-- | Update name (user changed name) and (if necessary) admins and
-- ignores.
updateName :: Msg -> Net ()
updateName msg = updateName' msg *> checkUpdate msg
 where
  -- | Check if an added/removed nick was subscribed to a list.  Update if
  -- this was the case.
  checkUpdate :: Msg -> Net ()
  checkUpdate Msg{ content = user } = do
    changeNameInSubs msg
    -- Check every channel for tells and print them if necessary.
    chans <- Map.keys <$> getChanOpts
    for_ chans \chan -> checkTells (msg & #sender  .~ user
                                        & #channel .~ chan)

  -- | Update the name of the user.
  updateName' :: ConfigWrite m => Msg -> m ()
  updateName' Msg{ sender = oldNick, content = newNick } =
    modifyOpts (Map.map updateNickIn)
   where
    updateNickIn :: Options -> Options
    updateNickIn opt@Options{ nicks }
      | oldNick `Set.member` nicks = opt & #nicks %~ (newNick `replaces` oldNick)
      | otherwise                  = opt

  changeNameInSubs :: ConfigWrite m => Msg -> m ()
  changeNameInSubs Msg{ sender = oldNick, content = newNick } =
      modifySubs (changeNameInSub oldNick newNick)

-- | Command for changing the default prefix of the bot.
changePrefix :: CmdMap
changePrefix = mkCmd $ defCmd
    { usage = "change-prefix"
    , help  = "Change one prefix into another one.  Usage: \
              \change-prefix <old-prefix> <new-prefix>"
    , execute = whenAdmin <*> runChangePrefix
    }

-- | Change one prefix into another one; @content@ should contain the
-- prefixes as follows: "<old-prefix> <SPACE> <new-prefix>"
runChangePrefix :: Msg -> Net ()
runChangePrefix Msg{ content, channel } = do
  let (old, n) = T.span (/= ' ') content
      new      = T.takeWhile (/= ' ') $ T.stripStart n
  opts <- getChanOpts
  when (channel `Map.member` opts) $
      modifyOpts $
          Map.adjust (#cmds %~ keyMap (tryChangePrefix old new)) channel
 where
  tryChangePrefix :: Text -> Text -> Command -> Command
  tryChangePrefix o n cmd
    | prefix cmd == o = cmd { prefix = n }
    | otherwise       = cmd

load :: CmdMap
load = mkCmd $ defCmd
  { usage   = "load"
  , help    = "Load (= enable) a command.  Admin command."
  , execute = whenAdmin <*> runLoad
  }
 where
  runLoad :: (ConfigRead m, ConfigWrite m) => Msg -> m ()
  runLoad = setCmdStatus True

unload :: CmdMap
unload = mkCmd $ defCmd
  { usage   = "unload"
  , help    = "Unload (= disable) a command.  Admin command."
  , execute = whenAdmin <*> runUnload
  }
 where
  runUnload :: (ConfigRead m, ConfigWrite m) => Msg -> m ()
  runUnload = setCmdStatus False

-- | Helper function for setting the command status to either be enabled
-- or disabled.
setCmdStatus :: (ConfigRead m, ConfigWrite m) => Bool -> Msg -> m ()
setCmdStatus status Msg{ content = cmd, channel } =
  getMatched channel cmd >>= \case
    Nothing   -> pass
    Just cmd' -> modifyOpts $
      Map.adjust (#cmds %~ modifyKey cmd' (#enabled .~ status))
                 channel
{-# SPECIALIZE setCmdStatus :: Bool -> Msg -> Net () #-}
