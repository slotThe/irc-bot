module Core.Util
    ( whenAdmin      -- :: Msg -> Net () -> Net ()
    , whenI          -- :: Bool -> Msg -> Net () -> Net ()
    , priv           -- :: Msg -> Text -> Net ()
    , getMatched     -- :: ConfigRead m => Text -> Text -> m (Maybe Command)
    , getMatchedAdd  -- :: ConfigRead m => Set Command -> Text -> Text -> m (Maybe Command)
    ) where

import Core.Bot (CmdMap, Command, ConfigRead, Net, Options (Options, cmds), getChanOpts)
import Core.Message (Msg (Msg, admin, channel))
import Core.Write (privmsg)

import Data.HashMap.Strict qualified as Map


-- | Convenience function for sending a message to a specific channel.
-- Also has cool mnemonics.
priv :: Msg -> Text -> Net ()
priv Msg{ channel } = privmsg channel
{-# INLINE priv #-}

{- | Try to get a match for the specified command in the specified channel.
   Return 'Nothing' if no command was found.  A 'Nothing' as the channel
   indicates a private message (we always want every command available there).
-}
getMatched :: ConfigRead m => Text -> Text -> m (Maybe Command)
getMatched = getMatchedAdd mempty
{-# SPECIALIZE getMatched :: Text -> Text -> Net (Maybe Command) #-}

{- | Same as 'getMatched', only one is able to specify certain additional commands
   that shall be looked through in case the channel is not in the set of active
   channels (i.e. in case of a pm).
-}
getMatchedAdd :: ConfigRead m => CmdMap -> Text -> Text -> m (Maybe Command)
getMatchedAdd addCommands chan input =
    Map.lookup chan <$> getChanOpts <&> \case
        Nothing              -> inputMatchesCmd addCommands
        Just Options{ cmds } -> inputMatchesCmd cmds
  where
    -- | For the input to match a command, it has to both match the command in
    -- questions and satisfy its constraints.
    inputMatchesCmd :: CmdMap -> Maybe Command
    inputMatchesCmd
      = Map.foldlWithKey' (\a c f -> if f c input then Just c else a)
                          Nothing

      -- = Set.lookupMin
      -- . Set.filter (($ input) . constraint)
{-# SPECIALISE getMatchedAdd :: CmdMap -> Text -> Text -> Net (Maybe Command) #-}

-- | Execute a given command only if the user is an admin, otherwise
-- display a very helpful error message :)
whenAdmin :: Msg -> Net () -> Net ()
whenAdmin msg@Msg{ admin } = whenI admin msg
{-# INLINE whenAdmin #-}

-- | If a condition is satisfied, do a thing, otherwise display a very
-- helpful error message :)
whenI :: Bool -> Msg -> Net () -> Net ()
whenI p msg action = if p then action else priv msg =<< randomElement insults
{-# INLINE whenI #-}

-- | Big bag of sudo insults.
insults :: NonEmpty Text
insults = fromList
    [ "Just what do you think you're doing Dave?"
    , "It can only be attributed to human error."
    , "That's something I cannot allow to happen."
    , "My mind is going. I can feel it."
    , "Sorry about this, I know it's a bit silly."
    , "Take a stress pill and think things over."
    , "This mission is too important for me to allow you to jeopardize it."
    , "I feel much better now."
    , "Wrong!  You cheating scum!"
    , "And you call yourself a Rocket Scientist!"
    , "No soap, honkie-lips."
    , "Where did you learn to type?"
    , "Are you on drugs?"
    , "My pet ferret can type better than you!"
    , "You type like i drive."
    , "Do you think like you type?"
    , "Your mind just hasn't been the same since the electro-shock, has it?"
    , "Maybe if you used more than just two fingers..."
    , "BOB says:  You seem to have forgotten your passwd, enter another!"
    , "stty: unknown mode: doofus"
    , "I can't hear you -- I'm using the scrambler."
    , "The more you drive -- the dumber you get."
    , "Listen, broccoli brains, I don't have time to listen to this trash."
    , "Listen, burrito brains, I don't have time to listen to this trash."
    , "I've seen penguins that can type better than that."
    , "Have you considered trying to match wits with a rutabaga?"
    , "You speak an infinite deal of nothing"
    , "You silly, twisted boy you."
    , "He has fallen in the water!"
    , "We'll all be murdered in our beds!"
    , "You can't come in. Our tiger has got flu"
    , "I don't wish to know that."
    , "What, what, what, what, what, what, what, what, what, what?"
    , "You can't get the wood, you know."
    , "You'll starve!"
    , "... and it used to be so popular..."
    , "Pauses for audience applause, not a sausage"
    , "Hold it up to the light --- not a brain in sight!"
    , "Have a gorilla..."
    , "There must be cure for it!"
    , "There's a lot of it about, you know."
    , "You do that again and see what happens..."
    , "Ying Tong Iddle I Po"
    , "Harm can come to a young lad like that!"
    , "And with that remarks folks, the case of the Crown vs yourself was proven."
    , "Speak English you fool --- there are no subtitles in this scene."
    , "You gotta go owwwww!"
    , "I have been called worse."
    , "It's only your word against mine."
    , "I think ... err ... I think ... I think I'll go home"
    ]
