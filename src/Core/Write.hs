module Core.Write
    ( privmsg  -- :: Text -> Text -> Net ()
    , write    -- :: IrcCommand -> Text -> Net ()
    ) where

import Core.Bot (Net, socket)
import Core.Parser.IrcMessage.Types (IrcCommand(PRIVMSG))

import Data.Text.IO qualified as T


-- | Send a message to the server the bot is currently connected to.
-- Also log it locally.
write :: IrcCommand -> Text -> Net ()
write cmd args = do
    h <- asks socket

    liftIO $ T.hPutStr h msg  -- Send message to the server
    putText $ "> " <> msg     -- Show sent message on the command line
  where
    -- Put the message together.
    msg :: Text
    msg = show cmd <> " " <> args <> "\r\n"

-- | Send a privmsg to the specified channel.
privmsg :: Text -> Text -> Net ()
privmsg chan content = write PRIVMSG (chan <> " :" <> content)
