module Prelude (
    -- * Re-exports
    module Exports,
    MonadUnliftIO,
    MonadThrow,
    toList,
    atomically,
    readTVarIO,
    catch,
    (%~),
    (.~),
    (<>~),
    (^.),
    (.:),

    -- * Exports
    randomElement, -- :: MonadIO m => NonEmpty a -> m a
    randomMember,  -- :: MonadIO m => Set a -> m a
    randomNumber,  -- :: (MonadIO m, Random a) => (a, a) -> m a
    replaces,      -- :: Ord a => a -> a -> Set a -> Set a
    decideEnding,  -- :: Text -> Text -> Text
    fst3,          -- :: (a, b, c) -> a
    snd3,          -- :: (a, b, c) -> b
    thrd3,         -- :: (a, b, c) -> c
    dropWhile1,    -- :: (Char -> Bool) -> Text -> Text
    numDecPlaces,  -- :: Double -> Int -> Double
    tmaybeRead,    -- :: Read a => Text -> Maybe a
    checkResult,   -- :: Status -> Bool
    both,          -- :: Applicative f => f Bool -> f Bool -> f Bool
    isEmpty,       -- :: Text -> Bool
    chunksOf,      -- :: Int -> Text -> [Text]
    keyMap,        -- :: (k -> l) -> HashMap k v -> HashMap l v
    modifyKey,     -- :: k -> (k -> k) -> HashMap k v -> HashMap k v
) where

import Relude               as Exports hiding (toList, atomically, readTVarIO)
import Relude.Extra.Newtype as Exports

import Control.Monad.Catch (MonadThrow)
import GHC.Exts (toList)
import UnliftIO (MonadUnliftIO)
import UnliftIO.Exception (catch)
import UnliftIO.STM (atomically, readTVarIO)

import Lens.Micro ((%~), (.~), (<>~), (^.))

-- Not re-exported
import Data.Set qualified             as Set
import Data.HashMap.Strict qualified  as Map
import Data.Text qualified            as T
import Relude.Unsafe qualified        as Unsafe

import Network.HTTP.Types.Status (Status, statusIsClientError, statusIsServerError)
import System.Random (Random, randomRIO)


-- | Multivariant composition!
infixr 8 .:
(.:) :: (a -> b) -> (c -> d -> a) -> c -> d -> b
(.:) = (.) . (.)
{-# INLINE (.:) #-}

-- | Pick a random element of a list.
-- (!!) is safe here.
randomElement :: forall a m. MonadIO m => NonEmpty a -> m a
randomElement list = (toList list Unsafe.!!) <$> randomIndex
 where
  randomIndex :: m Int
  randomIndex = randomNumber (0, length list - 1)
{-# INLINE randomElement #-}

-- | Pick a random element of a set.
randomMember :: forall a m. MonadIO m => Set a -> m a
randomMember set = randomIndex <&> (`Set.elemAt` set)
 where
  randomIndex :: m Int
  randomIndex = randomNumber (0, length set - 1)
{-# INLINE randomMember #-}

-- | Generate random number, then lift it into the bot's monad.
randomNumber :: (MonadIO m, Random a) => (a, a) -> m a
randomNumber = liftIO . randomRIO
{-# INLINE randomNumber #-}

-- | Helper function to delete on thing in a set and insert another.
replaces :: Ord a => a -> a -> Set a -> Set a
replaces new old = Set.insert new . Set.delete old
{-# INLINE replaces #-}

-- | Decide which ending to use after a "'".
-- TODO there has to be a better way to do this.
decideEnding :: Text -> Text -> Text
decideEnding firstName lastName
  | T.null lastName && T.last firstName == 's' = "'"
  | T.null lastName = "'s"
  | T.last lastName == 's' = "'"
  | otherwise = "'s"

-- | Extract the first element of a 3-tuple.
fst3 :: (a, b, c) -> a
fst3 (a, _, _) = a
{-# INLINE fst3 #-}

-- | Extract the second element of a 3-tuple.
snd3 :: (a, b, c) -> b
snd3 (_, b, _) = b
{-# INLINE snd3 #-}

-- | Extract the third element of a 3-tuple.
thrd3 :: (a, b, c) -> c
thrd3 (_, _, c) = c
{-# INLINE thrd3 #-}

-- | Drop until a certain character and then drop that character as well.
dropWhile1 :: (Char -> Bool) -> Text -> Text
dropWhile1 cb = T.drop 1 . T.dropWhile cb
{-# INLINE dropWhile1 #-}

-- | Round a number 'n' to exactly 'places' decimal places.
numDecPlaces :: Double -> Int -> Double
numDecPlaces n places = (fromInteger . round $ n * 10^places) / 10.0^^places

-- | Maybe read something that is a text.
tmaybeRead :: Read a => Text -> Maybe a
tmaybeRead = readMaybe . toString
{-# INLINE tmaybeRead #-}

-- | Don't get cut off while annoying everyone with highlights and long
-- messages in general.
chunksOf :: Int -> Text -> [Text]
chunksOf k = go
 where
  go :: Text -> [Text]
  go t | T.null a        = []
       | T.last a /= ' ' = (a <> c) : go d
       | otherwise       = a : go b
   where
    (a, b) = T.splitAt k t
    (c, d) = T.drop 1 <$> T.breakOn " " b

-- | Convenience lift of '(&&)' over two functions.
both :: Applicative f => f Bool -> f Bool -> f Bool
both = liftA2 (&&)
{-# INLINE both #-}

-- | Check for and indicate any errors in the http response.
checkResult :: Status -> Bool
checkResult status
  | statusIsServerError status = False
  | statusIsClientError status = False
  | otherwise                  = True

-- | Whether a string is empty, disregarding any white space.
isEmpty :: Text -> Bool
isEmpty = T.null . T.strip
{-# INLINE isEmpty #-}

-- | Map over the keys of a hash map.  O(n).
keyMap :: (Hashable k, Eq k, Hashable l, Eq l) => (k -> l) -> HashMap k v -> HashMap l v
keyMap f = fromList  . map (first f) . toList
{-# INLINE keyMap #-}

-- | Modify a certain key in a hash map, if it exists.  O(log n).
modifyKey :: (Hashable k, Eq k) => k -> (k -> k) -> HashMap k v -> HashMap k v
modifyKey k f hm = case hm Map.!? k of
  Nothing -> hm
  Just v  -> Map.insert (f k) v . Map.delete k $ hm
{-# INLINE modifyKey #-}
